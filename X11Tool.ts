
import Child from 'child_process'
import * as async from 'gh+/kwruntime/std@1.1.18/util/async.ts'

export class X11Tool {

    static $mouse: Mouse
    static get mouse():Mouse{
        if(!this.$mouse){
            this.$mouse = new Mouse()
        }
        return this.$mouse
    }


    static $paramsToArgs(args,params){
        for(let id in params){
            args.push("--" + id)
            if(params[id] !== null && params[id] !== true)
                args.push(params[id])
        }
    }

    static async sendKey(key: string){
        return await this.$getStd("xdotool",["key", key])
    }

    static exec(cmd, args, options = {}){
        return Child.spawn(cmd, args, options)
    }

    static sleep(time:number){
        return async.sleep(time)
    }
    static async search(params): Promise<Window[]>{

        let sync = params.sync
        let args = ["search"]
        if(params.sync){
            delete params.sync
        }
        this.$paramsToArgs(args,params)

        let response = await this.$getStd("xdotool", args)
        let data = Buffer.concat(response.stdout).toString()
        let lines = data.split(/\r?\n/g)
        let winds = []
        for(let line of lines ){
            if(/\d+/.test(line)){
                winds.push(new Window(Number(line.trim())))
            }
        }
        if(sync && !winds.length){
            await this.sleep(250)
            return await this.search(Object.assign(params,{sync: true}))
        }
        return winds

    }

    static async $getStd(cmd, args, options = undefined){
        let response = {
            stdout: [],
            stderr: []
        }
        let p = Child.spawn(cmd, args, options)
        let def = new async.Deferred<void>()
        p.on("exit", def.resolve)
        p.on("error", def.reject)
        p.stdout.on("data", function(b){
            response.stdout.push(b)
        })
        p.stderr.on("data", function(b){
            response.stderr.push(b)
        })
        await def.promise
        return response
    }

}

export interface Geometry{
    size: {
        width: number,
        height: number
    },
    location: {
        x: number,
        y: number
    },
    screen: number
}

export class Mouse{
    private windowid: string

    async move(x, y, params = null){
        let args = ["mousemove"]
        if(this.windowid){
            params = params || {}
            params.window = this.windowid
        }
        X11Tool.$paramsToArgs(args,params)
        args.push(String(x),String(y))
        await X11Tool.$getStd("xdotool",args)
    }

    async click(type= 1){
        await X11Tool.$getStd("xdotool",["click", String(type)])
    }

}


export class Window{
    id: string
    $mouse: Mouse
    constructor(id){
        this.id = id
    }

    get mouse(){
        if(!this.$mouse){
            this.$mouse = new Mouse()
            this.$mouse.windowid = this.id
        }
        return this.$mouse
    }

    async resize(width, height){
        await X11Tool.$getStd("xdotool",["windowsize", this.id.toString(), String(width), String(height)])
    }

    async move(x, y){
        await X11Tool.$getStd("xdotool",["windowmove", this.id.toString(), String(x), String(y)])
    }

    async activate(){
        await X11Tool.$getStd("xdotool",["windowactivate", this.id.toString()])
    }

    async getGeometry(){

        let response = await X11Tool.$getStd("xdotool",["getwindowgeometry", this.id.toString()])
        let reg1 = /Position:\s+(\d+)\,(\d+)\s+/
        let reg2 = /screen:\s+(\d+)/
        let reg3 = /Geometry:\s+(\d+)\x(\d+)/

        let data = Buffer.concat(response.stdout).toString()
        let lines = data.split(/\r?\n/g)
        let made = []
        let geometry = {
            size: {
                width: 0,
                height: 0
            },
            location: {
                x: 0,
                y: 0
            },
            screen: 0
        }

        for(let line of lines){
            if(!made[0]){
                let matchs =line.match(reg1)
                if(matchs){
                    geometry.location.x = Number(matchs[1])
                    geometry.location.y = Number(matchs[2])
                    made[0] = true
                }
            }
            if(!made[1]){
                let matchs =line.match(reg2)
                if(matchs){
                    geometry.screen = Number(matchs[1])
                    made[1] = true
                }
            }
            if(!made[2]){
                let matchs =line.match(reg3)
                if(matchs){
                    geometry.size.width = Number(matchs[1])
                    geometry.size.height = Number(matchs[2])
                    made[2] = true
                }
            }
        }
        return geometry

    }




}
