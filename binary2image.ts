import sharp from 'npm://sharp@0.30.4'
export interface Binary2ImageInterface{
	width: number
	height: number
	channels: number
	padding: number
	buffer: Buffer
}



export class Binary2Image implements Binary2ImageInterface{

	width: number
	height: number
	channels: number
	padding: number
	buffer: Buffer
	format: string 

	constructor(options:any){
		Object.assign(this, options)
	}

	async getBinary(){
		return await Binary2Image.imageBufferToBinary(this.buffer)
	}

	static async imageBufferToBinary(buffer: Buffer){
		let raw = sharp(buffer).raw()
		let image = await raw.toBuffer()
		let todel = image.readUInt16LE(0)
		image = image.slice(2, image.length - todel)
		return  image 
	}



	static async fromBuffer(content: Buffer, format = "png"){

		let start = Buffer.alloc(2)

		let channels = 4
		let len = content.length + 1
		
		/* Appropiate size of image */
		let width = parseInt(Math.sqrt((len + 2) / channels).toString())
		let height = width + 2
		if (width % 2 == 1) {
			width  += 1
			height -= 1
		}
		while((width * height * channels) < (len + 2)){
			height += 2
		} 


		let more = Buffer.allocUnsafe(0)
		let expected = width*height * channels
		if(content.length + 1 < expected){
			more = Buffer.alloc(expected - (start.length +  content.length))
			start.writeUInt16LE(more.length, 0)
		}
		let nbuf = Buffer.concat([start,content,more])
		let raw = new Binary2Image({
			width,
			height,
			format,
			channels,
			padding: more.length,
			buffer: null 
		})

		let img = sharp(nbuf,{
			raw:{
				width,
				height,
				channels
			}
		})
		
		if(format == "png")
			img = img.png({force: true, compressionLevel: 4})
		else if(format == "webp")
			img = img.webp({lossless:true, quality: 100, alphaQuality: 100, force: true})
		else if(format == "jp2")
			img = img.jp2({lossless:true, quality: 60, force: true})
		else if(format == "avif")
			img = img.avif({lossless:true, quality: 60, force: true})
		else if(format == "heif")
			img = img.heif({lossless:true, quality: 60, force: true})
		//console.info(buffer)
		raw.buffer = await img.toBuffer ()
		return raw
	}

	
}