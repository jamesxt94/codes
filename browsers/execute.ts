import Path from 'path'
import Os, { platform } from 'os'
import fs from 'fs'
import {find} from './find.ts'
import ChildProcess from 'child_process'
import {Exception} from "github://kwruntime/std@1.1.14/util/exception.ts"



export interface Icon{
	bytes: Buffer
	ext?: string 
}

export interface BrowserParams{

	path?: string 
	browser?: Array<string>
	icon?: string | Icon
	url: string 
	appId?: string
	appName?: string 
	size?: {
		width: number 
		height: number 
	}
	WMClass?: string 
	userDataDir?: string 
	args?: Array<string>
	mode?: 'app' | 'browser'
}


export async function execute(params: BrowserParams){
	let args = []
	let mode = params.mode || 'app'
	if(mode == 'app')
		args.push("--app=" + params.url)
	
	if(params.size){
		let w = String(params.size.width || 600)
		let h = String(params.size.height || 600)
		args.push(`--window-size=${w},${h}`)
	}	
	if(params.appId){
		if(!params.WMClass) params.WMClass = "kwrun_" + params.appId
		if(!params.userDataDir){
			// get a empty user-data-dir
			let folder = Path.join(Os.homedir(), ".kawi", "user-data")
			if(!fs.existsSync(folder)) fs.mkdirSync(folder)
			folder = Path.join(folder, "kwrun-browser-gui")
			if(!fs.existsSync(folder)) fs.mkdirSync(folder)
			folder = Path.join(folder, params.appId)
			if(!fs.existsSync(folder)) fs.mkdirSync(folder)
			params.userDataDir = folder
		}
	}
	if(params.WMClass){
		args.push("--class=" + params.WMClass)
	}
	if(params.userDataDir){
		args.push("--user-data-dir=" + params.userDataDir)
	}
	args.push("--no-first-run")
	if(Os.platform() != "win32"){
		if(process.getuid() == 0){
			args.push("--no-sandbox")
		}
	}
	if(params.args?.length){
		args.push(...params.args)
	}
	else{
		args.push("--no-default-browser-check")
		args.push("--disable-default-apps")
		args.push("--disable-popup-blocking")
		args.push("--disable-translate")
		args.push("--disable-background-timer-throttling")
	}

	let exe = params.path 
	if(params.browser){
		let result = await find(params.browser)
		if(!result.length)
			throw Exception.create("Failed to get a valid browser to start").putCode("BROWSER_NOT_FOUND")

		exe = result[0].location
	}

	// create .desktop if icon specified
	if(params.icon && (Os.platform() == "linux")){
		let iconFolder = Path.join(Os.homedir(), ".local", "share", "icons")
		let appFolder = Path.join(Os.homedir(), ".local", "share", "applications")
		if(fs.existsSync(iconFolder)){
			let ext = ".png"
			if(typeof params.icon == "string") ext = Path.extname(params.icon)
			else ext = params.icon.ext || ext 
			let deskid = "kwrun_browser_" + params.appId
			let iconid = deskid + ext
			deskid += ".desktop"
			let file = Path.join(iconFolder, iconid)
			if(typeof params.icon == "string"){
				let content = await fs.promises.readFile(params.icon)
				await fs.promises.writeFile(file, content)
			}
			else{
				await fs.promises.writeFile(file, params.icon.bytes)
			}

			// create desktop file
			if(fs.existsSync(appFolder)){

				let content = `[Desktop Entry]
NoDisplay=true
Icon=${file}
Type=Application
Categories=Application;
Exec="kwrun" %F
Name=${params.appName || params.appId}
StartupWMClass=${params.WMClass}
Comment=`
				
				file = Path.join(appFolder, deskid)
				await fs.promises.writeFile(file, content)

			}

		}
	}

	if(mode != "app")
		args.push(params.url)

	return ChildProcess.spawn(exe, args)
}