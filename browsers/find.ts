import Os from 'os'
import Path from 'path'
import fs from 'fs'
import {Exception} from "github://kwruntime/std@1.1.14/util/exception.ts"
import Child from 'child_process'
import * as async from "github://kwruntime/std@1.1.14/util/async.ts"

const platform = Os.platform()

interface Browsers{
	chromium?: string 
	
	brave?: stirng 
	braveBeta?: string

	chrome?: string	 
	chromeDev?: string
	chromeBeta?: string 

	msedge?: string 
	msedgeDev?: string 
	msedgeBeta?: string 

	yandex?: string 
	yandexBeta?: string
}

export interface BrowserInfo{
	name: string 
	location: string 
	version: SortableVersion
}


export interface SortableVersion{
	version: string 
	sortable?: string 
}

export function searchInPath(file: string){
	if(!process.env.PATH) return 
	let suffix = Os.platform() == "win32" ? ".exe" : ""
	let paths = process.env.PATH.split(Path.delimiter)
	for(let folder of paths){
		let exe = Path.join(folder, file) + suffix
		if(fs.existsSync(exe)) return exe
	}
}

export async function getVersion(browser: BrowserInfo): Promise<SortableVersion>{
	let def = new async.Deferred<void>()
	let bufs = []
	let p = Child.spawn(browser.location, ["--version"])
	p.on("error", def.reject)
	p.on("exit", def.resolve)
	p.stdout.on("data", function(bytes){
		bufs.push(bytes)
	})
	await def.promise
	let content = Buffer.concat(bufs).toString()
	let reg = /\s+([0-9\.]+)/
	let version = content.match(reg)?.[1]
	if(version){
		return {
			version,
			sortable: version.split(".").map((a) => a.padStart(5,'0')).join("")
		}
	}
}

export async function find(browsers: string[]){

	
	let files: Browsers  = {}
	let versions: {[key:string]: SortableVersion}  = {}
	if(platform == "linux"){

		files.chromium = searchInPath("chromium")
		if(!files.chromium){
			files.chromium = searchInPath("chromium-browser")
		}


		// possible values 
		files.chrome = 	searchInPath("google-chrome-stable")
		if(!files.chrome){
			if(fs.existsSync("/opt/google/chrome/google-chrome")){
				files.chrome = "/opt/google/chrome/google-chrome"
			}
		}

		files.chromeBeta = 	searchInPath("google-chrome-beta")
		if(!files.chromeBeta){
			if(fs.existsSync("/opt/google/chrome-beta/google-chrome")){
				files.chromeBeta = "/opt/google/chrome-beta/google-chrome"
			}
		}

		files.chromeDev = 	searchInPath("google-chrome-unstable")
		if(!files.chromeDev){
			if(fs.existsSync("/opt/google/chrome-unstable/google-chrome")){
				files.chromeDev = "/opt/google/chrome-unstable/google-chrome"
			}
		}

		files.brave = searchInPath("brave-browser-stable")
		if(!files.brave){
			if(fs.existsSync("/opt/brave.com/brave/brave")){
				files.brave = "/opt/brave.com/brave/brave"
			}
		}

		files.braveBeta = searchInPath("brave-browser-beta")
		if(!files.braveBeta){
			if(fs.existsSync("/opt/brave.com/brave-beta/brave")){
				files.braveBeta = "/opt/brave.com/brave-beta/brave"
			}
		}


		files.msedge = searchInPath("microsoft-edge-stable")
		if(!files.msedge){
			if(fs.existsSync("/opt/microsoft/msedge/msedge")){
				files.msedge = "/opt/microsoft/msedge/msedge"
			}
		}

		files.msedgeDev = searchInPath("microsoft-edge-dev")
		if(!files.msedgeDev){
			if(fs.existsSync("/opt/microsoft/msedge-dev/msedge")){
				files.msedgeDev = "/opt/microsoft/msedge-dev/msedge"
			}
		}

		files.msedgeBeta = searchInPath("microsoft-edge-beta")
		if(!files.msedgeBeta){
			if(fs.existsSync("/opt/microsoft/msedge-beta/msedge")){
				files.msedgeBeta = "/opt/microsoft/msedge-beta/msedge"
			}
		}

		files.yandex = searchInPath("yandex-browser-stable")
		if(!files.yandex){
			if(fs.existsSync("/opt/yandex/browser/yandex-browser")){
				files.yandex = "/opt/yandex/browser/yandex-browser"
			}
		}

		files.yandexBeta = searchInPath("yandex-browser-beta")
		if(!files.yandexBeta){
			if(fs.existsSync("/opt/yandex/browser-beta/yandex-browser")){
				files.yandexBeta = "/opt/yandex/browser-beta/yandex-browser"
			}
		}
		

	}
	else if(platform == "win32"){

		let execute = async function(bname, names, osuffix, prefixes){
			for(let name of names){
				let suffix = osuffix.replace("{name}", name)
				let options = [].concat(prefixes).map((a)=> Path.join(a, suffix))
				for(let option of options){
					if(fs.existsSync(option)){
						files[bname] = option

						let u = await fs.promises.readdir(Path.dirname(option))
						let vers = u.filter((a)=> /^[0-9\.]+$/.test(a)).map((a) => ({ version: a, sortable: a.split(".").map((b) => b.padStart(5,'0')).join("") }))
						versions[bname] = vers.sort((a,b) => a.sortable > b.sortable ? -1 : ((a.sortable < b.sortable) ? 1 : 0))[0]
						break 
					}
				}
			}
		}
		await execute("chrome", ["Chrome"], '\\Google\\{name}\\Application\\chrome.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])
		
		await execute("chromeBeta", ["Chrome Beta"], '\\Google\\{name}\\Application\\chrome.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])

		await execute("msedge", ["Edge"], '\\Microsoft\\{name}\\Application\\msedge.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])
		
		await execute("msedgeBeta", ["Edge Beta"], '\\Microsoft\\{name}\\Application\\msedge.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])

		await execute("msedgeDev", ["Edge Dev"], '\\Microsoft\\{name}\\Application\\msedge.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])
		
		await execute("brave", ["Brave-Browser"], '\\BraveSoftware\\{name}\\Application\\brave.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])
		
		await execute("braveBeta", ["Brave-Browser-Beta"], '\\BraveSoftware\\{name}\\Application\\brave.exe',
			[process.env.LOCALAPPDATA, process.env.PROGRAMFILES, process.env['PROGRAMFILES(X86)']])

	}
	else if(platform == "darwin"){

		// macos get browsers
		let options = [
			"/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary",
			Path.join(Os.homedir(), "Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.chromeDev = option 
				break 
			}
		}

		options = [
			"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
			Path.join(Os.homedir(), "Applications/Google Chrome.app/Contents/MacOS/Google Chrome")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.chrome = option 
				break 
			}
		}

		options = [
			"/Applications/Google Chrome Beta.app/Contents/MacOS/Google Chrome Beta",
			Path.join(Os.homedir(), "Applications/Google Chrome Beta.app/Contents/MacOS/Google Chrome Beta")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.chromeBeta = option 
				break 
			}
		}


		options = [
			"/Applications/Chromium.app/Contents/MacOS/Chromium",
			Path.join(Os.homedir(), "Applications/Chromium.app/Contents/MacOS/Chromium")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.chromium = option 
				break 
			}
		}

		options = [
			"/Applications/Brave Browser.app/Contents/MacOS/Brave Browser",
			Path.join(Os.homedir(), "Applications/Brave Browser.app/Contents/MacOS/Brave Browser")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.brave = option 
				break 
			}
		}

		options = [
			"/Applications/Brave Browser Beta.app/Contents/MacOS/Brave Browser Beta",
			Path.join(Os.homedir(), "Applications/Brave Browser Beta.app/Contents/MacOS/Brave Browser Beta")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.braveBeta = option 
				break 
			}
		}

		options = [
			"/Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge",
			Path.join(Os.homedir(), "Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.msedge = option 
				break 
			}
		}


		options = [
			"/Applications/Microsoft Edge Beta.app/Contents/MacOS/Microsoft Edge",
			Path.join(Os.homedir(), "Applications/Microsoft Edge.app/Contents/MacOS/Microsoft Edge Beta")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.msedgeBeta = option 
				break 
			}
		}

		options = [
			"/Applications/Microsoft Edge Dev.app/Contents/MacOS/Microsoft Edge Dev",
			Path.join(Os.homedir(), "Applications/Microsoft Edge Dev.app/Contents/MacOS/Microsoft Edge Dev")
		]
		for(let option of options){
			if(fs.existsSync(option)){
				files.msedgeDev = option 
				break 
			}
		}
		
	}	
	else{
		throw Exception.create(`Platform ${platform} is not supported`).putCode("NOT_SUPPORTED_EXCEPTION")
	}

	let info: Array<BrowserInfo> = []
	for(let browser of browsers){
		if(files[browser]){			
			info.push({
				name: browser,
				location: files[browser],
				version: versions[browser]
			})
		}
	}

	if(platform != "win32"){
		let tasks: Array<async.DelayedTask<SortableVersion>> = []
		for(let browser of info){
			// get version
			tasks.push(new async.DelayedTask<SortableVersion>(getVersion(browser)))
		}
		for(let browser of info){
			let task = tasks.shift()
			try{
				browser.version= await task.promise
			}catch(e){}
		}
	}

	return info 
}
