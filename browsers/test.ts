import {find} from './find.ts'
import {execute} from './execute.ts'
import fs from 'fs'

main()
async function main(){	
	let result = await find(["chrome","msedge","chromium", "brave", "chromeBeta","chromeDev", "msedgeBeta", "msedgeDev", "braveBeta"])	
	console.info("Results:", result)
}


async function testExecute(){	
	let p = await execute({
		url: "https://konta.kodhe.work",
		size: {
			width:690,
			height:800
		},
		browser: ["chrome","msedge", "chromium", "chromeBeta","msedgeBeta","chromeDev", "msedgeDev", "brave", "braveBeta"],
		appId: "konta",
		appName: "Konta"
	})
}
