import {kawix} from 'gh+/kwruntime/core@1.1.14/src/kwruntime.ts'

export interface CliParams{
	params: {[key: string]: any}
	paramsArray?: Array<{[key: string]: any}>
	values?: Array<any>
}

export function parseValue(value: string){

	if(value.startsWith("#number:")){
		return Number(value.substring(8))
	}

	if(value.startsWith("#boolean:")){
		return Boolean(Number(value.substring(9)))
	}

	if(value.startsWith("#date:")){
		let v:any = value.substring(6)
		if(/^\d+$/.test(v)) v = Number(v)
		return new Date(v)
	}

	if(value.startsWith("#string:")){
		return value.substring(8)
	}

	if(value.startsWith("#buffer:")){
		return Buffer.from(value.substring(8), 'base64')
	}

	if(value.startsWith("#json:")){
		return JSON.parse(value.substring(6))
	}

	if(value.startsWith("#list:")){
		return value.substring(6).split(",").map(parseValue)
	}

	if(value.startsWith("#array:") || (value == "#array")){
		return new Array(Number(value.substring(7)) || 0)
	}

	if(value == "#object"){
		return new Object()
	}

	return value 
}

export function parse(args?: string[]){

	if(!args) args = kawix.appArguments.slice(1)

	let params: {[key:string]: any} = {}
	let paramsArray: {[key:string]: any[]} = {}
	let values = [], disableValues = false

	let prefix = []

	for(let i=0;i<args.length;i++){
		let arg = args[i]
		let valid = arg.startsWith("--") || disableValues
		if(arg == "--"){
			disableValues = true 
			continue 
		}

		if(valid){
			let i = arg.indexOf("=")
			let name = arg.substring(disableValues ? 0 : 2)
			let value = null
			if(i >= 0){
				name = arg.substring(disableValues ? 0 : 2, i)
				value = parseValue(arg.substring(i+1))
			}
			
			
			if(name == ":close"){
				prefix.pop()
				continue 
			}

			if(prefix.length){
				
				
				let pa = prefix[prefix.length - 1]
				if(pa.type == "array"){
					value = name
					name = pa.name + String(pa.count++)
				}
				else{
					name = pa.name + name
				}
			}

			if(name == ":open"){
				name = value
				value = {}
				prefix .push({
					name: name + ":",
					type: 'object'
				})
			}
			
			if(name == ":aopen"){
				
				name = value
				value = []
				prefix .push({
					name: name + ":",
					type: 'array',
					count: 0
				})
				
			}

			
			

			let obj = params 
			if(name.indexOf(":") >= 0){
				let parts = name.split(":")
				for(let i=0;i< parts.length-1;i++){
					let part = parts[i]
					obj = obj[part] = obj[part] || {}
				}
				name = parts[parts.length - 1]
			}

			obj[name] = value
			if(obj === params){
				if(value instanceof Array){
					paramsArray[name] = value
				}
				else{
					if(paramsArray[name] === undefined) paramsArray[name] = []
					paramsArray[name].push(value)
				}
			}

		}else{
			values.push(parseValue(arg))
		}
	}

	let res = {
		params,
		paramsArray,
		values
	}
	return res 
}