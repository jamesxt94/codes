import {MongoClient, ObjectId as ObjectID, Db} from 'npm://mongodb@6.13.0'

import Path from 'path'
import Os from 'os'
import fs from 'fs'
import Child from 'child_process'

import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
export var ObjectId = ObjectID


export interface Config{
    database: string 
    autostart: boolean 
    listen? : number 
    timeout?: number 
    dbpath?: string 
    cachesize?: number 
}

export async function start(config){

    // connect ---> 
    let mongo = new MongoProxy(config)
    await mongo.connect()
    return mongo 
    
}


declare type InvokeCallback = (db: Db) => any
export class MongoProxy {

    #config : Config 
    #mongo: MongoClient
    constructor(config: Config){
        this.#config = config 
    }

    objectId(id?: string){
        return ObjectId(id)
    }

    async #startMongod(){
        let mb = 1024 *  1024 
        let gb = mb * 1024
        let mem = Math.max(Os.freemem() - (2300 * mb), 780 * mb) / 3
        if(this.#config.cachesize){
            mem = this.#config.cachesize 
        }
        let memStr = (mem / gb).toFixed(2);
        if(Os.platform() == "linux"){
            // start mongod 
            let datadir = this.#config.dbpath
            if(!datadir){
                datadir = Path.join(Os.homedir(), ".kawi")
                if(!fs.existsSync(datadir)) fs.mkdirSync(datadir)
                datadir = Path.join(datadir, "mongod")
                if(!fs.existsSync(datadir)) fs.mkdirSync(datadir)
                datadir = Path.join(datadir, String(this.#config.listen))
                if(!fs.existsSync(datadir)) fs.mkdirSync(datadir)
            }

            
            console.info("Starting mongod service")
            let def = new async.Deferred<void>()
            let p = Child.spawn("kmux", ["--id=services", "--server", "--background"], {
                stdio: 'inherit'
            })
            p.on("error", def.reject)
            p.on("exit", def.resolve)
            await def.promise 
            console.info("> KMUX Service started")
            await async.sleep(3000)

            def = new async.Deferred<void>()
            p = Child.spawn("kmux", ["--id=services", "--name=mongod-" + this.#config.listen,  "--start=mongod", "--arg=--dbpath", "--arg=" + datadir, "--arg=--port",
                "--arg=" + String(this.#config.listen), "--arg=--wiredTigerCacheSizeGB", "--arg=" + memStr], {
                stdio: 'inherit'
            })
            p.on("error", def.reject)
            p.on("exit", def.resolve)
            await def.promise 
            await async.sleep(1000)

            
        }
    }

    async connect(retry = 2){

        if(this.#config.autostart){
            // is local 
            console.info("Connecting")
            try{
                this.#mongo = await MongoClient.connect(this.#config.database, {
                    keepAlive: true,
                    connectTimeoutMS: 4000
                })
            }
            catch(e){

                console.info("Error generated:", e.message)
                // start automatically 
                if(retry == 0) throw e 
                await this.#startMongod()   
                return await this.connect(retry--)
            }
        }
        else{
            this.#mongo = await MongoClient.connect(this.#config.database, {
                keepAlive: true,
                connectTimeoutMS: this.#config.timeout || 5000
            })
        }
        
        this.#mongo.once("topologyClosed", ()=> this.#mongo = undefined)
        this.#mongo.once("close", ()=> this.#mongo = undefined)

    }

    async secureInvoke(callback: InvokeCallback){
        if(!this.#mongo){
            await this.connect()
        }
        try{
            return await callback(this.#mongo.db())
        }catch(e){
            if(e.message.indexOf("ECONNREFUSED") >= 0){
                this.#mongo = null 
            }
            throw e 
        }
    }

}
