
import * as git from "npm://isomorphic-git@1.21.0"
import http from "isomorphic-git/http/node"
import {parse} from "./cli-params.ts"
import fs from 'fs'
import crypto from 'crypto'


import Os from 'os'
import Path from 'path'

//https://gitlab.com/kodhework/packages

export class Program{

    static async main(args?: Array<string>){

        let cli = parse(args)
        let md5 = crypto.createHash("md5").update(cli.params.url).digest("hex")
        let folder = Path.join(Os.homedir(), ".kawi","user-data")
        if(!fs.existsSync(folder)) fs.mkdirSync(folder)
        folder = Path.join(folder, "com.kodhe.releases")
        if(!fs.existsSync(folder)) fs.mkdirSync(folder)
        folder = Path.join(folder, md5)
        

        if(!fs.existsSync(folder)){
            await git.clone({
                fs,
                http,
                dir: folder,
                gitdir: folder,
                url: cli.params.url,
                ref: "main"
            })
        }

    }


}
