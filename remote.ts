

import uniqid from "npm://uniqid@5.4.0"

import {Server} from 'gitlab://jamesxt94/mesha@ea813001/src/server.ts'
import {RPC, ShareSymbol} from 'gitlab://jamesxt94/mesha@ea813001/src/rpc.ts'

import {Exception} from "github://kwruntime/std@1.1.19/util/exception.ts"
import {parse} from './cli-params.ts'
import {kawix} from "github://kwruntime/core@b9d4a6a/src/kwruntime.ts"
import {Client} from 'gitlab://jamesxt94/mesha@ea813001/src/clients/default.ts' 
import machineId from 'npm://machine-uuid@1.2.0'
import Os from 'os'


const USER = (Os.userInfo()?.username || process.env.USER || 'default')

kawix["_vars"] = kawix["_vars"] || {}
if(kawix["_vars"]["com.kodhe.remote"] === undefined){
    kawix["_vars"]["com.kodhe.remote"] = {}
}
const remoteVars = kawix["_vars"]["com.kodhe.remote"]

if(!remoteVars.vars)
	remoteVars.vars = new Map<string, Map<string,any>>()	


if(!remoteVars.programs)
	remoteVars.programs = new Map<string, any>()	

if(!remoteVars.clients)
	remoteVars.clients = new Map<string, ClientExecutor>()


// proxy objects over client 
export class Proxied{
	target: any 
	constructor(target){
		this.target = target 
		this[Symbol('share')] = true 
	}

	invokeMethod(name: string, args){
		return this.target[name](...Array.from(args))
	}   	

	invokeProperty(name: string){
		return this.target[name]
	}

}

export class ClientExecutor{
	id = uniqid()
	$service: any
	$password: string 
	$clientId: string  

	constructor(){ 
		this[ShareSymbol] = true
		remoteVars.clients.set(this.id, this)
	}

	saveInfo(name: string, value:any){
		let s = remoteVars.vars.get(this.$clientId)
		if(!s){
			s = new Map<string, any>()
			remoteVars.vars.set(this.$clientId, s)
		}

		s.set(name, value)
	}


	getInfo(name: string){
		let s = remoteVars.vars.get(this.$clientId)
		if(!s){
			s = new Map<string, any>()
			remoteVars.vars.set(this.$clientId, s)
		}
		return s .get(name)
	}
	
	now(){
		return Date.now()
	}

	async getContextVar(context: string, varname: string){
		let c = await this.execute(`
		return this.getContext(arguments[0]).get(arguments[1])
		`, [context, varname])
		return c 
	}

	async getContextVarProxied(context: string, varname: string){
		let value = await this.getContextVar(context, varname)
		if(value && typeof value == 'object'){
			return new Proxied(value)
		}
		else{
			return value 
		}
	}

	async execute(code: string, params: any[]){
		if(!this.$service) throw Exception.create("Service execution not available").putCode("NOT_AVAILABLE")
		return await this.$service.execute(code, params)
	}

	setClient(clientId: string, service: any, password?: string){
		this.$service = service 
		this.$clientId = clientId 
		this.$password = password
		console.info("Client added:", clientId)

	}

	ping(){
		if(!this.$service) throw Exception.create("Service execution not available").putCode("NOT_AVAILABLE")
		return this.$service.ping()
	}

	async getContext(name: string){
		if(!this.$service) throw Exception.create("Service execution not available").putCode("NOT_AVAILABLE")
		return  await this.$service.getContext(name) 
	}


}

export class Executor{
	$vars = new Map<string, Map<string,any>>()	

	static $programs = remoteVars.programs
	static $vars = remoteVars.vars 

	rpc: RPC


	constructor(){ 
		this[ShareSymbol] = true
		
	}

	getContext(name: string){
		let c = Executor.$vars.get(name)
		if(!c){
			Executor.$vars.set(name, c = new Map<string, any>())
		}
		return c 
	}

	async execute(code: string, params: any[]){
		let func = Function(code)
		return await func.apply(this, params)
	}

	addProgram(id: string, value: any){
		Executor.$programs.set(id, value)
	}

	getClientIds(){
		
		
		let set = new Set<string>()
		let keys = remoteVars.clients.keys()
		for(let key of keys){
			let s = remoteVars.clients.get(key)
			set.add(s.$clientId)
		}
		return [...set]
	}

	async getClient(id: string, password: string){
		let keys = [... remoteVars.clients.keys()]
		keys.reverse()

		let client:ClientExecutor = null
		let selkey = ''
		for(let key of keys){
			let s = remoteVars.clients.get(key)
			if(s.$clientId == id){
				selkey = key 
				client = s 
				break 
			}
		}
		if(!client){
			throw Exception.create(`Not client with id ${id} available`).putCode("CLIENT_NOT_FOUND")
		}

		if(client.$password){
			if(client.$password !== password) return null
		}

		try{
			await client.ping()
		}catch(e){
			if(e.code == "RPA_DESTROYED" || e.code == "RPC_DESTROYED"){
				remoteVars.clients.delete(selkey)
				return await this.getClient(id, password)
			}
			throw e 
		}
		return client 
	}	

	ping(){
		return Date.now() 
	}
}

export class RemoteExecutor{
	static rpc: RPC
	

	static async connectMesha(address: string, password: string, reconnect: Function ){

		let client = new Client()
		await client.connect(address)

		let rpc = client.rpc 
		RemoteExecutor.rpc = rpc 
		let start = await rpc.defaultScope.remote("GetObject")		
		rpc.stream.on("close", ()=>{
			console.info("connection closed")
			if(reconnect){
				reconnect()
			}
		})
		rpc.on("error", console.error)
		client.on("error", console.error)
		return  await start.invoke(password)
	}
}




export class Program{
	static async main(args: Array<string>){
		try{
			let cli = parse(args || kawix.appArguments)

			if(cli.params.client !== undefined){

				let executor = new Executor()
				let remote:ClientExecutor = await RemoteExecutor.connectMesha(cli.params.address, cli.params.password, Program.main.bind(null, args))
				let clientId = USER + "::" + (await machineId()) + "::" + cli.params.id 
				clientId = cli.params["client-id"] || clientId
				await remote.setClient(clientId, executor, cli.params["client-password"] || cli.params.password)

				console.info()
				console.info("> Connected to server:",  cli.params.address)
				console.info("> My ID", clientId)

			}
			else{

				let shared = function(rpc: RPC, password: string){
					if(password === cli.params.password){
						let exe= new Executor()
						exe.rpc = rpc 
						return exe 
					}
					if(password === cli.params["client-password"]){
						return new ClientExecutor()
					}

					let client = Executor.$programs.get(password)
					if(client){
						if(client.$create){
							return client.$create()
						}
						else{
							return client
						}
					}
				}
				
				let meshaBind = cli.params["mesha-bind"]
				if(!(meshaBind instanceof Array))
					meshaBind = cli.paramsArray["mesha-bind"]
				
				let servers = [], addresses = []
				if(meshaBind){	
					for(let bind of meshaBind){
						let server = new Server()
						server.on("error", console.error)
						server.on("rpc", (rpc: RPC)=>{
							rpc.add("GetObject", shared.bind(null, rpc))
							
							rpc.on("close", ()=>{
								console.info("> Closing client.")
								rpc = null
							})
							
						})
						addresses.push(await server.bind(bind))
						servers.push(server)
					}
				}

				console.info("> Listening:", addresses)

				// evitar que se cierre el proceso
				process.on("uncaughtException", function(e){
					console.error("> --- Ocurrió un error fatal en la aplicación:", e)
					console.error("-------------------")
				})

				return {
					addresses,
					servers
				}
			}


		}catch(e){
			console.error("Error:", e)
		}
	}
}


