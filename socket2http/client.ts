import net from 'net'
import http from 'http'
import https from 'https'


import {Exception} from "github://kwruntime/std@aa7aa4f/util/exception.ts"
import * as async from "github://kwruntime/std@aa7aa4f/util/async.ts"


export class Client{
	#server: net.Server

	#url: string 
	#address: string 
	#method: string 

	#socketDataIn = new Map<string, Buffer>()


	connect(url: string, address: string, method = 'large'){
		this.#url = url 
		this.#address = address
		this.#method = method 
	}


	async getJsonResponse(url: string){
		let mod = url.startsWith("https:") ? https : http 
		let def = new async.Deferred<any>()
		let req = mod.get(url, async (res) => {
			let chunks = []
			for await (let chunk of res){
				chunks.push(chunk)
			}	

			let content = Buffer.concat(chunks).toString()
			if(res.statusCode != 200){
				let e = JSON.parse(content).error || {}
				let ex = Exception.create("Error from request:" + (e.message || "Unknown")).putCode(e.code)
				Object.assign(ex, e)
				return def.reject(ex)
			}
			
			return def.resolve(JSON.parse(content))
		})
		req.on("error", function(){})
		return await def.promise
	}


	async getResponse(url: string){
		let mod = url.startsWith("https:") ? https : http 
		let def = new async.Deferred<any>()
		let req = mod.get(url, async (res) => {
			let chunks = []
			for await (let chunk of res){
				chunks.push(chunk)
			}	

			//console.info("Res status code:", res.statusCode, res.headers)
			let content = Buffer.concat(chunks)
			if(res.statusCode != 200){
				let e = JSON.parse(content.toString()).error || {}
				let ex = Exception.create("Error from request:" + (e.message || "Unknown")).putCode(e.code)
				Object.assign(ex, e)
				return def.reject(ex)
			}
			
			return def.resolve(content)
		})
		req.on("error", function(){})
		return await def.promise
	}

	async #getReceiverStream(url: string){
		let mod = url.startsWith("https:") ? https : http 
		let def = new async.Deferred<any>()
		let req = mod.get(url, async (res) => {
			if(res.statusCode != 200){
				let chunks = []
				for await (let chunk of res){
					chunks.push(chunk)
				}	
				let content = Buffer.concat(chunks).toString()

				let e = JSON.parse(content).error || {}
				let ex = Exception.create("Error from request:" + (e.message || "Unknown")).putCode(e.code)
				Object.assign(ex, e)
				def.reject(ex)
				return 
			}
			def.resolve(res)
		})
		req.on("error", function(){})
		return await def.promise
	}


	#getSenderStream(url: string, callback = undefined, length = 0){
		let mod = url.startsWith("https:") ? https : http 
		let h = {
			"content-type": "application/octect-stream",
			"content-encoding": "chunked"
		}
		if(length > 0) h["content-length"] = String(length)

		return mod.request(url, {
			method: 'PUT',
			headers: h
		}, callback)
	}

	#getAddress(address: string){
		
		address = address.replace("unix+", "unix://")
		address = address.replace("tcp+", "tcp://")

		if(address.startsWith("tcp://")){
			let parts = address.substring(6).split(":")
			return {
				port: Number(parts[1]),
				host: parts[0]
			}
		}
		else if(address.startsWith("unix://")){
			return {
				path: address.substring(7)
			}
		}

		else if(/^\d+$/.test(address)){
			return {
				port: Number(address),
				host: "127.0.0.1"
			}
		}
		else{
			throw Exception.create("Invalid address: " + address).putCode("INVALID_ADDRESS")	
		}

	}


	async listen(address: string){

		
		let server = this.#server = net.createServer(async (socket) => {

			socket.pause()
			socket.on("error", function(){})

			try{
				let uri = `${this.#url}/hexconnect/${Buffer.from(this.#address).toString('hex')}`
				// first get uid 
				let data = await this.getJsonResponse(uri)
				let streamuri = `${this.#url}/data/${data.uid}`
				console.info("here:", streamuri)

				if(this.#method == "large"){
					let receiver = await this.#getReceiverStream(streamuri)
					let sender = await this.#getSenderStream(streamuri)

					receiver.on("error", function(e){
						console.info("Error on receiving data:", e.message)
					})
					sender.on("error", function(e){
						console.info("Error on sending data:", e.message)
					})

					receiver.once("close", function(){
						socket.destroy()
					})
					sender.once("close", function(){
						socket.destroy()
					})

					receiver.pipe(socket)
					socket.pipe(sender)

				}
				else{


					console.info("> Connection created with id:", data.uid)
					uri = `${this.#url}/byportion/${data.uid}`
					// first get uid 
					await this.getJsonResponse(uri)

					// start write and read by portions
					socket.on("data", (bytes) => {
						let buf = this.#socketDataIn.get(data.uid)
						if(buf){
							buf = Buffer.concat([buf, bytes])
						}
						else{
							buf = bytes 
						}
						//console.info("To write: ", bytes)
						this.#socketDataIn.set(data.uid, buf)
						//if(buf.length > (5*1024*1024)) socket.pause()
					})

					this.#startRead(socket, this.#url, data.uid)
					this.#startWrite(socket, this.#url, data.uid)

					socket.on("destroy", () => {
						console.info("> Connection closed with id:", data.uid)
						this.#socketDataIn.delete(data.uid)
					})

				}
				
				socket.resume()
			}catch(e){
				socket.destroy()
				console.error("Failed connect:",e)
			}
	
		})

		let info = await this.#getAddress(address)
		let onlisten = function() {
			let addr = server.address()
			console.log((new Date()) + ' Server is listening on port', addr?.port || addr);
		}

		if(info.port){
			this.#server.listen(info.port, info.host, onlisten)
		}
		else if(info.path){
			this.#server.listen(info.path, onlisten)
		}
	}

	async #startRead(socket, url: string, uid: string ){
		try{
			while(true){

				if(socket.destroyed){
					break 
				}

				try{
					let bytes = await this.getResponse(`${url}/read/${uid}`)
					console.info("Bytes readed:", bytes)
					if(socket.destroyed){
						break 
					}
					socket.write(bytes)
				}catch(e){

					console.info("Ecode:", uid, e.code)
					if(e.code == "SOCKET_NOT_FOUND") {
						socket.destroy()
					}

					else {
						console.error("Failed read:", e.message)
					}
				}
				
			}
		}catch(e){
			console.error("Error on read:", e.message)
		}

		try{
			await this.getResponse(`${url}/close/${uid}`)
		}catch(e){
			console.error("Failed send close:", e.message)
		}

	}


	async #startWrite(socket, url: string, uid: string ){
		try{
			while(true){

				if(socket.destroyed){
					break 
				}

				let bytes = this.#socketDataIn.get(uid)
				if(!bytes){
					await async.sleep(10)
					continue 
				}
				this.#socketDataIn.delete(uid)

				console.info("Bytes writed:", bytes)
				let def = new async.Deferred<void>()
				let stream = this.#getSenderStream(`${url}/write/${uid}`, async function(res){

					//console.info("Writed response:", res.statusCode)
					let chunks = []
					for await (let chunk of res){
						chunks.push(chunk)
					}	

					if(res.statusCode != 200){
						let content = Buffer.concat(chunks).toString()
						
						let e = JSON.parse(content).error || {}
						let ex = Exception.create("Error from request:" + (e.message || "Unknown")).putCode(e.code)
						Object.assign(ex, e)
						return def.reject(ex)
					}
					def.resolve()
				}, bytes.length)
				stream.write(bytes)
				stream.on("error", () => {
					socket.destroy()
					def.resolve()
				})
				stream.end()

				try{
					await def.promise
					socket.resume()
				}catch(e){

					console.info("Ecode:", uid,  e.code)
					if(e.code == "SOCKET_NOT_FOUND") {
						socket.destroy()
					}

					else {
						let cbytes = this.#socketDataIn.get(uid)
						if(cbytes){
							cbytes = Buffer.concat([bytes, cbytes])
						}
						else{
							cbytes = bytes 
						}
						this.#socketDataIn.set(uid, cbytes)
						console.error("Failed read:", e.message)
					}
				}
			}	
		}catch(e){
			console.error("Error on read:", e.message)
		}


		try{
			await this.getResponse(`${url}/close/${uid}`)
		}catch(e){
			console.error("Failed send close:", e.message)
		}

	}

}