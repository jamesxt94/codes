import {Client} from './client.ts'
import {Server} from './server.ts'
import {parse} from "gitlab://jamesxt94/codes@aec85a5d/cli-params.ts"
import {kawix} from "github://kwruntime/core@8196c24/src/kwruntime.ts"

export class Program{

	static async main(args: Array<string>){
		

		try{
			let cli = parse(args || kawix.appArguments)
			if(cli.params.server !== undefined){

				let s = new Server()
				await s.start(cli.params.address)

			}	
			else{ 

				let c = new Client()
				c.connect(cli.params.url, cli.params.destination, cli.params.mode || "large")
				await c.listen(cli.params.address)


			}
		}catch(e){
			console.error("Failed execute:", e.message)
		}

		
	}

}