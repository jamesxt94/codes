import {Server as httpServer} from "github://kwruntime/std@aa7aa4f/http/server.ts"
import {Router} from "github://kwruntime/std@aa7aa4f/http/router.ts"
import {Exception} from "github://kwruntime/std@aa7aa4f/util/exception.ts"
import * as async from "github://kwruntime/std@aa7aa4f/util/async.ts"
import {JsonParser} from 'github://kwruntime/std@aa7aa4f/http/parsers/json.ts'
import { HttpContext , Reply} from "github://kwruntime/std@aa7aa4f/http/context.ts"
import { ServerResponse } from 'http'
import uniqid from 'npm://uniqid@5.4.0'

import net from 'net'

export class Server{	
	#server: httpServer
	#router: Router 
	#sockets = new Map<string, net.Socket>()
	#map = new Map<string, string>()

	#socketDataIn = new Map<string, Buffer>()
	#socketDataOut = new Map<string, Buffer>()


	constructor(){}

	get router(){
		return this.#router 
	}

	get portMaps(){
		return this.#map
	}


	async start(address: string ){

		this.#server = new httpServer()
		await this.#server.listen(address)
		console.info("Server started:", this.#server.address)

		this.#server.bodyParsers.set("application/json", JsonParser)
        let routerb = new Router()
		let router = new Router()
        routerb.catchNotFound = true 
        routerb.catchUnfinished = true

		routerb.use("", router)
		routerb.get("/", this.#status.bind(this))

		router.all("/connect/*", this.#connect.bind(this))
		router.all("/hexconnect/:hexaddress", this.#connect.bind(this))

		router.all("/byportion/:uid", this.#byPortions.bind(this))
		router.get("/read/:uid", this.#readPortion.bind(this))
		router.put("/write/:uid", this.#writePortion.bind(this))
		router.all("/close/:uid", this.#close.bind(this))

		router.get("/data/:uid", this.#startGetData.bind(this))
		router.put("/data/:uid", this.#startPutData.bind(this))


		this.#router = routerb

		let write = ServerResponse.prototype.write
        ServerResponse.prototype.write = function(data, callback){
            if(!this.writable || this.writableEnded){
                if(callback) callback()
                return 
            } 
            return write.apply(this, arguments)
        }
		let send = Reply.prototype.send 
		Reply.prototype.send = function(){
			if(this.raw.writableEnded) return 

			return send.apply(this, arguments)
		} 


        this.#httpBeginAccept()


	}

	async #status(context: HttpContext){
		return await context.reply.send({
			"status": "ok",
			"message": "Http to Socket service online",
			file: import.meta.url
		})
	}

	#getAddress(address: string){
		
		address = address.replace("unix+", "unix://")
		address = address.replace("tcp+", "tcp://")

		if(address.startsWith("tcp://")){
			let parts = address.substring(6).split(":")
			return {
				port: Number(parts[1]),
				host: parts[0]
			}
		}
		else if(address.startsWith("unix://")){
			return {
				path: address.substring(7)
			}
		}

		else if(/^\d+$/.test(address)){
			return {
				port: Number(address),
				host: "127.0.0.1"
			}
		}
		else{
			throw Exception.create("Invalid address: " + address).putCode("INVALID_ADDRESS")	
		}

	}


	async #connect(context: HttpContext){

		let addr = context.request.params.wild
		if(context.request.params.hexaddress){
			addr = Buffer.from(context.request.params.hexaddress,"hex").toString()
		}
		let item = this.portMaps.get(addr)
		if(item) addr = item 

		let info = await this.#getAddress(addr)

		let socket:net.Socket				
		if(info.port){
			socket = net.createConnection(info.port, info.host)
		}
		else if(info.path){
			socket = net.createConnection(info.path)
		}
		socket.on("error", function(){})
		socket.on("close", () => {	
			//console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');		
			//connection.close()
			this.#sockets.delete(uid)
			this.#socketDataOut.delete(uid)
			this.#socketDataIn.delete(uid)
		})

		socket.pause()

		let uid = uniqid()
		this.#sockets.set(uid, socket)

		return await context.reply.send({
			uid
		})
	}

	async #byPortions(context: HttpContext){
		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}

		socket.on("data", (bytes) => {
			let buf = this.#socketDataIn.get(uid)
			if(buf){
				buf = Buffer.concat([buf, bytes])
			}
			else{
				buf = bytes 
			}
			this.#socketDataIn.set(uid, buf)

			if(buf.length > (5*1024*1024)) socket.pause()
		})

		socket.resume()
		return context.reply.send({
			ok: true
		})
	}

	async #close(context: HttpContext){
		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}
		socket.destroy()

		return context.reply.send({
			ok: true
		})
	}

	async #readPortion(context: HttpContext){
		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}

		let bytes = this.#socketDataIn.get(uid)
		while(!bytes){
			await async.sleep(10)
			bytes = this.#socketDataIn.get(uid)
		}

		context.reply.code(200).header("content-type","application/octect-stream")
			.header("content-length", String(bytes.length))
		let promise = context.reply.send(bytes)
		this.#socketDataIn.delete(uid)
		socket.resume()
		return await promise 
	}


	async #writePortion(context: HttpContext){

		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}

		let def = new async.Deferred<void>()
		let stream = context.request.body.stream
		stream.on("error", function(e){
			console.error("> Error from socket:", uid, e.message)
		})
		stream.on("close", function(e){
			def.resolve()
		})
		stream.on("data", function(bytes){
			socket.write(bytes)
		})

		stream.on("end", function(){
			def.resolve()
		})

		await def.promise
		return await context.reply.send({
			ok: true 
		})

	}
	



	async #startPutData(context: HttpContext){

		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}

		let def = new async.Deferred<void>()
		let stream = context.request.body.stream
		stream.pipe(socket)
		stream.on("error", function(e){
			console.error("> Error from socket:", uid, e.message)
		})
		stream.on("close", function(e){
			socket.destroy()
			def.resolve()
		})

		socket.once("close", function(){
			stream.destroy()
		})
		await def.promise
	}

	async #startGetData(context: HttpContext){

		let uid = context.request.params.uid
		let socket = this.#sockets.get(uid)
		if(!socket){
			throw Exception.create("Socket not found").putCode("SOCKET_NOT_FOUND")
		}

		let def = new async.Deferred<void>()
		let stream = context.reply.raw
		socket.pipe(stream)
		stream.on("error", function(e){
			console.error("> Error from socket:", uid, e.message)
		})
		stream.on("close", function(e){
			socket.destroy()
			def.resolve()
		})

		socket.once("close", function(){
			stream.destroy()
		})

		socket.resume()

		context.reply.raw.statusCode = 200 
		context.reply.raw.setHeader("content-type","application/octect-stream")
		context.reply.raw.write(Buffer.allocUnsafe(0))
		await def.promise

	}





	async #httpBeginAccept(){
        let iterator = this.#server.getIterator(["error","request"])
		for await(let event of iterator){
			try{
				if(event.type == "error"){
					console.error("> Error from webserver:", event.data.message)
				}
				else {
					this.#router.lookup(event.data)
				}
			}catch(e){
				console.error("> Failed to accept requests:", e.message)
			}
		}
    }

}