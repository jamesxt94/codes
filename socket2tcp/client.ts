
import {client as WebSocketClient} from 'npm://websocket@1.0.34'
import net, { AddressInfo } from 'net'
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"

// this script read from a websocket and transform into a TCP Socket server 
// usage (example):
// kwrun client --url=wss://localhost:8000 --address=tcp://127.0.0.1:2222

export class Program{
	static main(){

		let params = {}		
		for(let i=1;i<kawix.appArguments.length;i++){
			let arg = kawix.appArguments[i]
			let parts = arg.split("=")
			let name = parts[0].substring(2)

			let value = parts.slice(1).join("=") || ''
			params[name] = value
			params[name+"_Array"] = params[name+"_Array"] || []
			params[name+"_Array"].push(value)
	
		}
		
		
		if(params.url && params.address){
			let tcp = new WSocketToTCPClient(params)
			tcp.create()
			tcp.listen()
		}

	}
}

export class WSocketToTCPClient{
	#params: any 
	#server: net.Server
	#count: number = 0

	constructor(params: any){
		this.#params = params
	}

	create(){


		let add = ()=> this.#count++
		let sub = ()=> this.#count--
		let count = ()=> this.#count
		let url = this.#params.url 

		let server = this.#server = net.createServer(function(socket){

			socket.pause()
			socket.on("error", function(){})
			// connect to websocket 
			var client = new WebSocketClient()
			client.on('connectFailed', function(error) {
				console.error("Error connect:", error)
				if(!socket.destroyed) socket.destroy()
			})
	
			let wpaused = false 
			client.on('connect', function(connection) {
				
				add()
				console.info("> New connection. Open connections:", count())
				connection.on('error', function(error) {
					console.log("Connection Error: " + error.toString());
				});
				connection.on('close', function() {
					sub()
					console.info("> Connection closed. Open connections:", count())
					if(!socket.destroyed) socket.destroy()
				});
				connection.on('message', function(message) {
	
					//console.info("message:", message)
					let data = null 
					if (message.type === 'utf8') {
						data = Buffer.from(message.utf8Data)
					}
					else{
						data = message.binaryData
					}
	
					if(socket.write(data) === false){
						connection.pause()
						wpaused= true 
						socket.once("drain", ()=>{
							connection.resume()
							wpaused= false
						})
					}
				})
	
				socket.on("close", function(){
					connection.close()
				})
				
				socket.on("data", function(bytes){
					//console.info("Received..", bytes)
					socket.pause()
					connection.sendBytes(bytes, ()=> socket.resume())
				})
				socket.resume()
			})
	
			
			client.connect(url, 'echo-protocol')
	
		})
	}

	listen(){
		let server = this.#server 
		let address : string = this.#params.address 
		let onlisten = function() {
			let addr = server.address()
			console.log((new Date()) + ' Server is listening on port', (addr as AddressInfo).port || addr);
		}

		if(address.startsWith("tcp://")){
			let parts = address.substring(6).split(":")
			this.#server.listen(Number(parts[1]), parts[0]|| "127.0.0.1", onlisten)
		}
		else if(address.startsWith("unix://")){
			this.#server.listen(address.substring(7), onlisten)
		}

		else if(/^\d+$/.test(address)){
			this.#server.listen(Number(address), "127.0.0.1", onlisten)
		}
		else{
			throw Exception.create("Invalid address: " + address).putCode("INVALID_ADDRESS")	
		}
	}


}
