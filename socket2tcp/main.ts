import {Program as ProgramClient} from './client.ts'
import {Program as ProgramServer} from './server.ts'

export class Program{

	static main(){
		// read arguments 
		
		let params:any = {}
		for(let i=1;i<kawix.appArguments.length;i++){
			let arg = kawix.appArguments[i]
			let parts = arg.split("=")
			let name = parts[0].substring(2)

			let value = parts.slice(1).join("=") || ''
			params[name] = value
			params[name+"_Array"] = params[name+"_Array"] || []
			params[name+"_Array"].push(value)
		}
		
		if(params.server !== undefined){
			return ProgramServer.main(params)
		}

		else if((params.client !== undefined) || (params.connect !== undefined)){
			return ProgramClient.main(params)
		}

	}

}