import {server as WebSocketServer} from 'npm://websocket@1.0.34'
import http from 'http'
import net, { AddressInfo } from 'net'
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"
import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import {EventEmitter} from 'events'



export class Program{
	static main(){

		let params = {}		
		for(let i=1;i<kawix.appArguments.length;i++){
			let arg = kawix.appArguments[i]
			let parts = arg.split("=")
			let name = parts[0].substring(2)

			let value = parts.slice(1).join("=") || ''
			params[name] = value
			params[name+"_Array"] = params[name+"_Array"] || []
			params[name+"_Array"].push(value)
	
		}

		params.address = params.address || params.bind
		if(params.address){
			if(!params.password){
				console.error("> You need specify a password")
				process.exit(1)
			}
			let tcp = new WSocketToTCPServer(params)

			tcp.createHttpServer()
			tcp.createWebSocket()
			tcp.listen()
		}

	}
}

export class WSocketToTCPServer extends EventEmitter{
	#params: any 
	//#server: net.Server
	#count: number = 0
	#maps= new Map<string, string>()
	#http: http.Server

	constructor(params: any){
		super()
		this.#params = params
	}

	setHttpServer(server){
		this.#http = server 
	}

	createHttpServer(){
		var server = this.#http = http.createServer(function(request, response) {
			//console.log((new Date()) + ' Received request for ' + request.url)
			response.writeHead(200)
			response.write("Service available")
			response.end()
		})
		server.on("error", console.error)
	}

	get maps(){
		return this.#maps
	}

	#getAddress(address: string){
		
		address = address.replace("unix+", "unix://")
		address = address.replace("tcp+", "tcp://")

		if(address.startsWith("tcp://")){
			let parts = address.substring(6).split(":")
			return {
				port: Number(parts[1]),
				host: parts[0]
			}
		}
		else if(address.startsWith("unix://")){
			return {
				path: address.substring(7)
			}
		}

		else if(/^\d+$/.test(address)){
			return {
				port: Number(address),
				host: "127.0.0.1"
			}
		}
		else{
			throw Exception.create("Invalid address: " + address).putCode("INVALID_ADDRESS")	
		}

	}

	#getRedirection(path: string){
		let ev = {
			address: null,
			path
		}
		
		this.emit("path", ev)
		return ev.address
	}

	createWebSocket(){


		let password = this.#params.password 
		let maps = this.#maps
		let path = this.#params.path  || ''


		const wserver = new WebSocketServer({
			httpServer: this.#http,
			// You should not use autoAcceptConnections for production
			// applications, as it defeats all standard cross-origin protection
			// facilities built into the protocol and the browser.  You should
			// *always* verify the connection's origin and decide whether or not
			// to accept it.
			autoAcceptConnections: false
		})
		//wserver.on("error", console.error)
	
		function originIsAllowed(origin) {
			// put logic here to detect whether the specified origin is allowed.
			console.info("Origin:", origin)
			return true;
		}
	
		let getAddress = this.#getAddress.bind(this)
		let getRedirection = this.#getRedirection.bind(this)
		wserver.on('request', function(request) {

			let reqpath = request.resourceURL.pathname
			if(path && !reqpath.startsWith(path)) request.reject()

			try{
				
				let address = getRedirection(reqpath)
				if(!address){
					let parts = reqpath.substring(path.length).split("/").filter(Boolean)
					if(parts[0] != password){
						request.reject()
					}
					let ad = parts[1]
					if(maps.get(ad)){
						ad = maps.get(ad)
					}
					address = getAddress(ad)
				}

				let connection = request.accept('echo-protocol', request.origin)
				let wpaused = false
				let socket:net.Socket				
				if(address.port){
					socket = net.createConnection(address.port, address.host)
				}
				else if(address.path){
					socket = net.createConnection(address.path)
				}
				socket.on("error", function(){})
				socket.on("close", function(){	
					console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');		
					connection.close()
				})
				connection.on("close", function(){
					if(!socket.destroyed)
						socket.destroy()
				})
	
				connection.on('message', function(message) {
					let data = null 
					if (message.type === 'utf8') {
						//console.log('Received Message: ' + message.utf8Data);
						//connection.sendUTF(message.utf8Data);
						data = Buffer.from(message.utf8Data)
	
					}
					else if (message.type === 'binary') {
						//console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
						//connection.sendBytes(message.binaryData);
						data = Buffer.from(message.binaryData)
					}
					if(socket.write(data) === false){
						if(!wpaused){
							connection.pause()
							wpaused = true
							socket.once("drain", ()=>{
								connection.resume()
								wpaused = false
							})
						}
					}
				})
	
				socket.on("data", function(bytes){
					//console.info("data", bytes)
					socket.pause()
					connection.sendBytes(bytes, ()=> socket.resume())
				})
			}
			catch(e){
				console.error(e)
			}
	
		})
	}

	async listen(){
		let server = this.#http
		let address = this.#getAddress(this.#params.address)
		let def = new async.Deferred<any>()
		let onlisten = function() {
			let addr = server.address()
			//console.log((new Date()) + ' Server is listening on port', (addr as AddressInfo).port || addr)
			def.resolve(addr)
		}

		server.once("error", def.reject)
		if(address.host){
			server.listen(address.port, address.host, onlisten)
		}
		else if(address.path){
			server.listen(address.path, onlisten)
		}
		else{
			throw Exception.create("Invalid address: " + this.#params.address).putCode("INVALID_ADDRESS")	
		}
		return await def.promise
	}


}
