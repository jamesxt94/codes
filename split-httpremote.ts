import {parse} from './cli-params.ts'
import Net from 'net'
import {Exception} from "gh+/kwruntime/std@1.1.19/util/exception.ts"
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import os from 'os'
import crypto from 'crypto'
import Path from 'path'
import fs from 'fs'

export class Program{
    static net: Net.Server
    static params:any 

    static httpaddr: any  
    static remoteaddr: any

    static async main(args:Array<string>){

        this.net = Net.createServer(this.#listener.bind(this))
        
        

        let cli = parse()    
        if(cli.params.address){
            this.params = cli.params
            
            
            this.httpaddr = this.#getAddress(this.params.http)
            this.remoteaddr = this.#getAddress(this.params.remote)
        
            

            await this.listen(cli.params.address)
            console.info("> Listening:", this.#getAddress(cli.params.address))
        }


    }

    static getpath(addressInfo:any){
        let addr = ''
        if(os.platform() == "win32"){
            if(addressInfo.type == "win32-pipe"){
                addr = addressInfo.path
            }
            else{
                let prefix = addressInfo.type == "local" ? "com.kodhe.mesha-" : ""
                addr = "\\\\.\\pipe\\" + crypto.createHash('md5').update(prefix + addressInfo.path).digest("hex")
            }
        }
        else{
            if(addressInfo.type == "local"){
                let file = Path.join(os.homedir(), ".kawi", "sockets")
                if(!fs.existsSync(file)) fs.mkdirSync(file)
                file = Path.join(file, addressInfo.path)
                addr = file 
            }
        }
        return addr 
    }

    static async listen(address:  string ){
        let net = this.net 

        let def = new async.Deferred<any>()
        net.once("error", def.reject)
        net.once("listening", def.resolve)



        let addressInfo = this.#getAddress(address)
        let addr = '' 

        

        if(addressInfo.host){
            net.listen(addressInfo.port, addressInfo.host)
        }

        if(addressInfo.path){
            addr = this.getpath(addressInfo)

            if(os.platform() != "win32"){
                let sock:Net.Socket
                try{                    
                    if(fs.existsSync(addr)){

                        sock = new net.Socket()
                        let def = new async.Deferred<void>()                        
                        sock.once("error", def.reject)
                        sock.once("connect", def.resolve)
                        sock.connect(addr)
                        await def.promise

                    }
                    
                }catch(e){
                    if((e.code == "ECONNREFUSED") || (e.code == "ENOENT")){
                        await fs.promises.unlink(addr)
                    }
                    else{
                        throw e 
                    }
                }
                finally{
                    if(sock){
                        sock.destroy()
                    }
                }
            }

            net.listen(addr)

        }

        await def.promise 

    }

    static #listener(socket){

        let httpaddr = this.httpaddr
        let remoteaddr = this.remoteaddr
        
        socket.once("data", function(bytes){

            let s = bytes.toString("binary")
            

            if(s.indexOf("HTTP")>=0){

                console.info("> Detected input HTTP connection")

                let sock2     
                if(httpaddr.path){
                    let path = this.getpath(httpaddr)
                    sock2 = Net.createConnection(path)
                }
                else{
                    sock2 = Net.createConnection(httpaddr.port, httpaddr.host)
                }
                sock2.once("error", ()=>socket.destroy())
                sock2.on("error",  console.error)
                sock2.on("connect", function(){
                    sock2.write(Buffer.from(s,"binary"))
                    socket.pipe(sock2)
                    sock2.pipe(socket)
                })
            }
            else{

                console.info("> Detected input TCP connection")

                let sock2     
                if(remoteaddr.path){
                    let path = this.getpath(remoteaddr)
                    sock2 = Net.createConnection(path)
                }
                else{
                    sock2 = Net.createConnection(remoteaddr.port, remoteaddr.host)
                }
                sock2.once("error",()=> socket.destroy())
                sock2.on("error",  console.error)
                sock2.on("connect", function(){
                    sock2.write(Buffer.from(s,"binary"))
                    socket.pipe(sock2)
                    sock2.pipe(socket)
                })

            }

        })

    }

    static #getAddress(address: string){
		
		address = address.replace("unix+", "unix://")
		address = address.replace("tcp+", "tcp://")

		if(address.startsWith("tcp://")){
			let parts = address.substring(6).split(":")
			return {
				port: Number(parts[1]),
				host: parts[0]
			}
		}
		else if(address.startsWith("unix://")){
			return {
				path: address.substring(7)
			}
		}

        else if(address.startsWith("\\\\.\\pipe\\")){
			return {
                type: "win32-pipe",
				path: address
			}
		}

        else if(address.startsWith("local+")){
            
            return {
                type: "local",
                path: address.substring(6)
            }

        }

		else if(/^\d+$/.test(address)){
			return {
				port: Number(address),
				host: "127.0.0.1"
			}
		}
		else{
			throw Exception.create("Invalid address: " + address).putCode("INVALID_ADDRESS")	
		}

	}


}