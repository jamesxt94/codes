import {Program as Capturer} from 'gitlab://kodhework/x11grab@b82f357/main.ts'
// Script para redireccionar la pantalla 
import Path from 'path'
import Os from 'os'
import Child, { ChildProcess } from 'child_process'

import {Client} from 'gitlab://jamesxt94/mesha@2.0.3/src/clients/default.ts' 
import { RPC, ShareSymbol } from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts'

//import { Readable, Writable } from 'gitlab://jamesxt94/mesha@3a6411d/Stream.ts'

import {parse} from 'gitlab://jamesxt94/codes@465eb5ab/cli-params.ts'
import * as async from 'gh+/kwruntime/std@1.1.19/util/async.ts'
import fs, { lstat } from 'fs'
//import {start, MongoProxy} from "gitlab://jamesxt94/codes@465eb5ab/mongodb/start.ts"
import Zlib from 'zlib'
import crypto from 'crypto'
import uniqid from 'npm://uniqid@5.4.0'
import byteStr from 'npm://bytes@3.1.2'
import {terminal} from "npm://terminal-kit@2.4.0"
import machineUid from 'npm://machine-uuid@1.2.0'
import {Exception} from "github://kwruntime/std@1.1.18/util/exception.ts"
import minimatch from "npm://minimatch@5.1.0"


let bytes = byteStr
export interface SyncResponse{
    folders?: Array<any>
    files?: Array<any>
    ignored?: Array<any>
}

export interface SyncFolderConfig{
    path: string 
    stats: {[key: string]: any}
    maxsize?: number
}

let share = Symbol("share")
export class RemoteExecutor{
    static rpc: RPC
  
    static async connectMesha(address: string, password: string){
        const client = new Client()
        await client.connect(address)
		const rpc = RemoteExecutor.rpc = client.rpc
        
	
		let start = await rpc.defaultScope.remote("GetObject")
		return  await start.invoke(password)
	}
}


let version = 10073
export var kawixDynamic = {
    time: 10000
}

export class FileSync{
    #remote: any 
    #remoteObject: FileSync
    static from = "local"
    static #syncs = new Map<string, FileSync>()
    params: any 
    paramsArray: any
    #p: ChildProcess
    //#streams = new Map<string, Writable>()

    constructor(){
        this[share] = true 
    }

    static get version(){
        return version
    }

    static getSync(uid: string){
        let sync = this.#syncs.get(uid)
        if(!sync){
            sync = new FileSync()
            this.#syncs.set(uid, sync)
        }
        return sync 
    }

    get from(){
        return FileSync.from
    }

    get version(){
        return version
    }

    get remote(){
        return this.#remoteObject
    }

    async addRemote(remote: FileSync){
        if(this.#remoteObject){
            try{
                await this.#remoteObject.version 
                throw Exception.create(`Another client is in use. Only 1 client for machine allowed`).putCode("CLIENT_NOT_AVAILABLE")
            }
            catch(e){
                if(e.code == "RPA_DESTROYED"){
                    this.#remoteObject = null 
                }
                else{
                    throw e 
                }
            }
        }
        this.#remoteObject = remote
    }


    setParams(params: any, paramsArray: any){
        this.params = params
        this.paramsArray = paramsArray
    }


    #getStr(func:Function){
        let str = func.toString()
        str = str.substring(str.indexOf("{")+1)
        str = str .substring(0, str.lastIndexOf("}"))
        return str 
    }

 

    async connect(address: string, password: string){
        this.#remote = await RemoteExecutor.connectMesha(address, password) 
    }



    async prepare(){

        let content = ''
		let url = import.meta.url 
		if(url.startsWith("file:")){
			content = await fs.promises.readFile(__filename, 'utf8')
            url = ''
		}
        
        let str = this.#getStr(function(){


            let ex = async ()=>{
                var context = this.getContext("com.kodhe.scripts");
                var content = arguments[0]
                var waitedVersion = arguments[1]
                var url = arguments[2]

                var objClass = context.get("FileSync-Class1")
                if(!objClass || (objClass.version < waitedVersion)) {
                    await (async function(){
                        var outf = url
                        if(!outf){
                            var _Path = await global["kawix"].import("path")
                            var _fs = await global["kawix"].import("fs")
                            var _Os = await global["kawix"].import("os")
                            var userdata = _Path.join(_Os.homedir(),".kawi","user-data")
                            if(!_fs.existsSync(userdata)) _fs.mkdirSync(userdata)
                            userdata = _Path.join(userdata, "com.kodhe.scripts")
                            if(!_fs.existsSync(userdata)) _fs.mkdirSync(userdata)
                            outf = _Path.join(userdata, "filesync" + waitedVersion + ".ts")
                            _fs.writeFileSync(outf, content)
                        }
                        var mod = await global["kawix"].import(outf)
                        objClass = mod.FileSync
                        objClass.from = "remote"
                        context.set("FileSync-Class1", objClass)
                    })();            
                }

                return new objClass()
            }
            return ex()
            
        })
        this.#remoteObject = await this.#remote.execute(str, [content, version, url])
        console.info("FileSync version=", await this.#remoteObject)
        console.info()
        await this.#remoteObject.addRemote(this)
        await this.#remoteObject.setParams(this.params, this.paramsArray)
    }


    async getEnv(){
        if(this.from == "local"){
            return await this.#remoteObject.getEnv()
        }
        var env =  process.env
        var nvar = {}
        var entries = Object.entries(env)
        entries.sort((a,b) => (a[0] > b[0]) ? 1 : ((a[0] < b[0]) ? -1 : 0))
        for(let item of entries){
            nvar[item[0]] = item[1]
        }
        return nvar
    }



    async getMachineUid(folder: string){
        if(folder){
            if(!fs.existsSync(folder)) fs.mkdirSync(folder)
            let syncfolder = Path.join(folder, ".sync-data")
            if(!fs.existsSync(syncfolder)) fs.mkdirSync(syncfolder)
            let uidfile = Path.join(syncfolder, "uid.txt")
            let uid = ''
            if(!fs.existsSync(uidfile)){
                uid = uniqid()
                await fs.promises.writeFile(uidfile, uid)
            }  
            else{
                uid = (await fs.promises.readFile(uidfile,'utf8')).split("\n")[0]
            }
            return uid
        }
        return await machineUid()
    }

    async getCache(folder: string, uid: string = ''){

        if(!fs.existsSync(folder)) fs.mkdirSync(folder)
        let syncfolder = Path.join(folder, ".sync-data")
        if(!fs.existsSync(syncfolder)) fs.mkdirSync(syncfolder)

        
        let data:any = {}
        let filedata = Path.join(syncfolder, uid + ".brotli")
        if(fs.existsSync(filedata)){
            let content = await fs.promises.readFile(filedata)
            let bytes = Zlib.brotliDecompressSync(content)
            data = JSON.parse(bytes.toString())
        }
        else{
            filedata = Path.join(syncfolder, uid + ".json")
            if(fs.existsSync(filedata)){
                let content = await fs.promises.readFile(filedata, "utf8")
                data = JSON.parse(content)
            }
        }
        return data 
    }

    async writeCache(folder: string, cache: any, uid: string = ''){

        if(!fs.existsSync(folder)) fs.mkdirSync(folder)
        let syncfolder = Path.join(folder, ".sync-data")
        if(!fs.existsSync(syncfolder)) fs.mkdirSync(syncfolder)

        
        
        //if(!uid) uid = await this.remote.getMachineUid(folder)
        
        let filedata = Path.join(syncfolder, uid + ".brotli")
        let text = JSON.stringify(cache)
        let bytes = Zlib.brotliCompressSync(Buffer.from(text), {
            params: {
                [Zlib.constants.BROTLI_PARAM_MODE]: Zlib.constants.BROTLI_MODE_GENERIC,
                [Zlib.constants.BROTLI_PARAM_QUALITY]: 4
            }
        }) 
        await fs.promises.writeFile(filedata, bytes)

        let filejson = Path.join(syncfolder, uid + ".json")
        if(fs.existsSync(filejson)) await fs.promises.unlink(filejson)
    }

    async writeStats(folder: string, stats: any, uid: string = ''){

        if(!fs.existsSync(folder)) fs.mkdirSync(folder)
        let syncfolder = Path.join(folder, ".sync-data")
        if(!fs.existsSync(syncfolder)) fs.mkdirSync(syncfolder)        
        
        //if(!uid) uid = await this.remote.getMachineUid(folder)

        let cache = await this.getCache(folder, uid)
        
        cache.stats = Object.assign(cache.stats || {}, stats)
        let entries = Object.entries<any>(cache.stats)
        for(let [key,value] of entries){
            if(value.type == "delete"){
                delete cache.stats[key]
            }
        }

        /*let filedata = Path.join(syncfolder, uid + ".json")
        await fs.promises.writeFile(filedata, JSON.stringify(cache, null, '\t'))
        */

        let filedata = Path.join(syncfolder, uid + ".brotli")
        let text = JSON.stringify(cache, null, '\t')
        let bytes = Zlib.brotliCompressSync(Buffer.from(text), {
            params: {
                [Zlib.constants.BROTLI_PARAM_MODE]: Zlib.constants.BROTLI_MODE_GENERIC,
                [Zlib.constants.BROTLI_PARAM_QUALITY]: 4
            }
        }) 
        await fs.promises.writeFile(filedata, bytes)
    }

    consoleLog(...args:Array<any>){
        if(this.from == "remote"){
            return this.remote.consoleLog(...args)
        }
        else{
            return console.log.apply(console, args)
        }
    }

    termAction(action: string, args: Array<any>){
        return terminal[action].apply(terminal, args)
    }

    terminalUp(){
        let s = (" ").repeat(process.stdout.columns)
        terminal.up()
        console.info(s)
        terminal.up()
    }

    async bidirectionalSync(localpath: string, remotepath: string){


        let remoteUid = await this.remote.getMachineUid(remotepath)
        let localUid = await this.getMachineUid(localpath)

        let localcache = await this.getCache(localpath, remoteUid)
        let remotecache = await this.remote.getCache(remotepath, localUid)

        let remoteResponse = await this.remote.syncFolder({
            path: remotepath,
            stats: localcache.stats || {}
        })

        let localResponse = await this.syncFolder({
            path: localpath,
            stats: remotecache.stats || {}
        })

        //console.info("LOCAL", localResponse.files, "REMOTE", remoteResponse.files)
        
        
        await this.createFolders(localpath, remoteResponse)
        await this.remote.createFolders(remotepath, localResponse)
        

        let localChangesByName = {}
        for(let file of localResponse.files){
            localChangesByName[file.name] = file
        }

        let changes = [], lstats: any = {}
        for(let file of remoteResponse.files){
            if(localChangesByName[file.name]){
                //this.consoleLog("> File modified on source, ignored:", file.name)
                lstats[file.name] = localChangesByName[file.name]
            }
            else{
                changes.push(file)                
                lstats[file.name] = file
            }
            
            //rstats[file.name] = file
        }


        
        let todel1 = changes.filter((a) => a.type == 'delete')
        let todel2 = localResponse.files.filter((a) => a.type == 'delete')
        
        let ignored = [...localResponse.ignored, ...remoteResponse.ignored]
        
        
        if(ignored.length){
            await this.consoleLog("> Ignored files")
            for(let item of ignored){
                await this.consoleLog("  \x1b[33m" + item.name, "\x1b[0m") 
            }
        }
        //process.exit(1)

        await this.consoleLog("> Remote changes:", changes.length ? "" : "None")
        for(let change of changes){
            if(change.type == "delete"){
                await this.consoleLog("  \x1b[31m" + change.name, "\x1b[0mDeleted") 
            }
            else{
                await this.consoleLog("  \x1b[34m" + change.name + "\x1b[0m", change.directory ? "Folder" : ("Size="+change.size) )
            }                
        }
        await this.consoleLog("> Local changes:", localResponse.files.length ? "" : "None")
        for(let change of localResponse.files){
            //if(change.name.indexOf("node_modules") >= 0) process.exit(2)
             
            if(change.type == "delete"){
                await this.consoleLog("  \x1b[31m" + change.name, "\x1b[0mDeleted") 
            }
            else{ 
                await this.consoleLog("  \x1b[34m" + change.name + "\x1b[0m", change.directory ? "Folder" : ("Size="+change.size) )
            }
        }

        

        
        let ignores1 = await this.createFiles(localpath, remotepath, changes, remoteUid)
        let ignores2 = await this.remote.createFiles(remotepath, localpath, localResponse.files, localUid)        


        let syncStats1 = Object.assign({}, localChangesByName, lstats)
        let syncStats2 = Object.assign({}, localChangesByName, lstats)

        syncStats1 = await this.normalizeStats(syncStats1, ignores2)
        syncStats2 = await this.normalizeStats(syncStats2, ignores1)


        // folders 
        for(let folder of localResponse.folders){
            syncStats1[folder.name] = folder 
            syncStats2[folder.name] = folder 
        }
        for(let folder of remoteResponse.folders){
            syncStats1[folder.name] = folder 
            syncStats2[folder.name] = folder 
        }
        

        await this.writeStats(localpath, syncStats1,remoteUid)
        await this.remote.writeStats(remotepath, syncStats2, localUid)
        await this.consoleLog("> Sync finished")
    }

    async normalizeStats(syncStats1: any, ignores1: Array<any>){
        for(let ignore of ignores1){
            let stat = null 
            try{
                stat = await fs.promises.lstat(ignore.path)
            }
            catch(e){}
            if(stat){
                let link = ''
                if(stat.isSymbolicLink()){
                    link = await fs.promises.readlink(ignore.path)
                }
                syncStats1[ignore.name] = {
                    name:ignore.name,
                    type: 'create',
                    mtimeMs: Math.round(stat.mtimeMs),
                    atimeMs: Math.round(stat.atimeMs),
                    mode: stat.mode,
                    size: stat.size,
                    link
                }
            }
        }
        return syncStats1
    }

    /*
    async createWriteStream(){
        let stream : Writable
        if(this.from == "local"){
            stream = await RemoteExecutor.rpc.createWriteStream()
        }
        else{
            stream = await RPC.createWriteStream()
        }
        let uid = stream.id
        this.#streams.set(uid, stream)
        return uid
    }


    async createReadStream(uid: string){
        let stream : Readable
        if(this.from == "local"){
            stream = await RemoteExecutor.rpc.createReadStream(uid)
        }
        else{
            stream = RPC.createReadStream(uid)
        }  
        return stream 
    }
    */

    async getFileContents(paths: Array<string>, sw: any){
        //let sw = this.#streams.get(uid)
        let contentInfo = []
        let er = null
        //let sha1 = crypto.createHash('md5')
        
        let brotliStream = Zlib.createBrotliCompress({
            params: {
                [Zlib.constants.BROTLI_PARAM_MODE]: Zlib.constants.BROTLI_MODE_GENERIC,
                [Zlib.constants.BROTLI_PARAM_QUALITY]: 4
            }
        })
        try{
            sw.on("error", console.error)

            
            brotliStream.on("error", (e) => er = e)
            brotliStream.pipe(sw)
            let offset = 0, text = ''

            for(let path of paths){
                
                let bytes = await fs.promises.readFile(path)
                contentInfo.push({
                    path,
                    offset, 
                    size: bytes.length
                })
                offset+= bytes.length

                let nt = "Packing content " + byteStr(offset)
                if(nt != text){
                    ;await (this.from == "remote" ? this.remote : this).terminalUp()
                    ;await (this.from == "remote" ? this.remote : this).consoleLog(nt)
                    text = nt
                }

                if(sw.destroyed) break 
                if(brotliStream.destroyed) break 
                if(brotliStream.write(bytes) === false){
                    let def = new async.Deferred<void>()
                    brotliStream.once("drain", def.resolve)
                    await def.promise
                }
                
            }
            brotliStream.end()

            if(er) throw er 
        }catch(e){
            brotliStream.destroy()
            sw.destroy()
            throw e 
        }
        finally{
            //this.#streams.delete(uid)
        }
        return contentInfo
    }



    stdoutWrite(...content: Array<Buffer | string>){
        for(let i=0;i<content.length;i++){
            if(i > 0) process.stdout.write(" ")
            process.stdout.write(content[i])            
        }
    }

    stderrWrite(...content: Array<Buffer | string>){
        for(let i=0;i<content.length;i++){
            if(i > 0) process.stdout.write(" ")
            process.stderr.write(content[i])            
        }
    }


    async createBinaryStream(){
        let binary = await RemoteExecutor.rpc.createBinaryStream() 
        return {
            [ShareSymbol]: true,
            local: binary.remote,
            remote: binary.local
        }
    }

    async createFiles(localpath: string, remotepath: string, changes: Array<any>, rid: string ){

        let ignores = []
        let linkChanges = changes.filter((a) => (a.type == "create") && a.link)
        let fileChanges = changes.filter((a) => (a.type == "create") && (!a.directory) && (!a.link))
        let deletes = changes.filter((a) => (a.type == "delete"))

        // get data of file changes 
        let fd = null
        let readContent = null 
        let outfile = ''

        if(fileChanges.length){
            ;(this.from == "remote" ? this.remote : this).consoleLog("Packing content ...")    
            

            let er = null 
            let def = new async.Deferred<void>()
            let task = new async.DelayedTask<void>(def.promise)
            //let uid = await this.remote.createWriteStream()

            let binary: any = null 
            if(this.from == "local"){
                binary = await RemoteExecutor.rpc.createBinaryStream()
            }
            else{
                let res = await this.remote.createBinaryStream()
                binary = {
                    local: await res.local,
                    remote: await res.remote 
                }
            }
            

            let uid = uniqid()
            let sr = binary.local
            let dec = Zlib.createBrotliDecompress()
            sr.on("error", (e) => er = e)
            dec.on("error", (e) => er = e)
            outfile = Path.join(localpath, ".sync-data", rid)
            if(!fs.existsSync(outfile)){
                await fs.promises.mkdir(outfile)
            }
            else{
                let files = await fs.promises.readdir(outfile)
                files.sort()
                while(files.length > 1){
                    let name = files.shift()
                    let ufile = Path.join(outfile, name)
                    try{
                        await fs.promises.unlink(ufile)
                    }catch(e){}
                }
            }
            outfile = Path.join(outfile, uid + ".stream")
            let fw = fs.createWriteStream(outfile)
            sr.pipe(dec)
            dec.pipe(fw)
            fw.on("error", (e) => er = e)
            fw.on("finish", def.resolve)

            let contentInfoArr = await this.remote.getFileContents(fileChanges.map((a) => Path.posix.join(remotepath, a.name)), binary.remote)
            if(er) throw er 
            await task.promise
            let stat1 = await fs.promises.stat(outfile)


            let contentInfo = {}
            for(let item of contentInfoArr){
                contentInfo[item.path] = item
            }
            fd = await fs.promises.open(outfile, "r")
            readContent = async (path) => {
                let item = contentInfo[path]
                if(!item) throw Exception.create(`Failed get content of path: ${path}`).putCode("PATH_NOT_FOUND")
                
                let res = await fd.read({
                    buffer: Buffer.allocUnsafe(item.size),
                    length: item.size,
                    position: item.offset
                })
                if(res.bytesRead != item.size){
                    throw Exception.create(`Failed get content on ${path}. Expected bytes: ${item.size}, eader: ${res.bytesRead}`)
                }
                let bytes = res.buffer.slice(0, item.size)
                if(bytes.length != item.size){
                    throw Exception.create(`Failed get content on ${path}. Expected bytes: ${item.size}, eader: ${bytes.length}`)
                }
                return bytes 
            }
        }

        
        try{
            
            let items1 = await this.$createFiles(localpath, remotepath, linkChanges)
            let items2 = []
            if(fileChanges.length){
                items2 = await this.$createFiles(localpath, remotepath, fileChanges, readContent)
            }
            let items3 = await this.$createFiles(localpath, remotepath, deletes)

            return [...items1, ...items2, ...items3]
        }catch(e){
            throw e 
        }
        finally{
            if(fd){
                await fd.close()
                if(outfile) await fs.promises.unlink(outfile)
            }
        }
    }
    async $createFiles(localpath: string, remotepath: string, changes: Array<any>, readContent = null){
        let ignores = []
        for(let file of changes){            

            let lpath = Path.posix.join(localpath, file.name)
            if(file.type == "create"){
                
				let cstat = null 
				try{
					cstat = await fs.promises.lstat(lpath)
				}catch(e){}


                

                if(file.link){
                    
					if(cstat)
						await fs.promises.unlink(lpath)

					
                    let isfolder = false 

                    
                    if(Os.platform() == "win32"){
                        let target = Path.resolve(Path.dirname(lpath), file.link)
                        try{                            
                            if(fs.existsSync(target)){
                                let stat = await fs.promises.stat(target)
                                isfolder = stat.isDirectory()
                            }
                            else{
                                console.error("> Failed detect if symlink can be created. Path=", lpath, "Target=", file.link)    
                            }
                        }catch(e){
                            console.error("> Failed detect if symlink can be created. Path=", lpath, "Target=", file.link, e.message)    
                        }
    


                        if(isfolder){
                            // make a junction 
                            try{
                                await fs.promises.symlink(file.link, lpath, "junction")
                            }catch(e){
                                console.error("> Failed creating junction. Path=", lpath, "Target=", file.link, e.message)
                                ignores.push({
                                    name: file.name,
                                    path: lpath
                                })
                            }
                        }
                        else{
                            await fs.promises.writeFile(lpath, `[SYMLNK]\n${file.link}`)
                        }
                        //await fs.promises.utimes(lpath, file.atimeMs, file.mtimeMs)
                    }
                    else{
                        try{
                            try{
                                await fs.promises.symlink(file.link, lpath)
                            }catch(e){
                                await fs.promises.symlink(localpath, lpath)
                            }
                        }catch(e){
                            ignores.push({
                                name: file.name,
                                path: lpath
                            })
                        }
                    }
                }else{
                    let bytes = null
                    if(cstat){
                        if(cstat.isSymbolicLink()){
                            await fs.promises.unlink(lpath)
                        }
                        else if(cstat.isDirectory()){
                            await fs.promises.rm(lpath, {recursive: true})
                        }
                    }

                    if(readContent){
                        bytes = await readContent(Path.posix.join(remotepath, file.name))
                    }
                    else{
                        bytes = await this.remote.readFileContent(Path.posix.join(remotepath, file.name), true )
                        if(bytes){
                            bytes = Zlib.brotliDecompressSync(bytes)
                        }
                        bytes = bytes || Buffer.allocUnsafe(0)
                    }

                    await fs.promises.writeFile(lpath, bytes)
                    
                    
                    if(Os.platform() != "win32")
                        await fs.promises.chmod(lpath, file.mode)

                    await fs.promises.utimes(lpath, new Date(file.atimeMs), new Date(file.mtimeMs))
                }
            }
            else if(file.type == "delete"){
                let stat = null
                try{
                    //console.info("> Deleting file:", file.name)
                    stat = await fs.promises.lstat(lpath)
                    
                }catch(e){
                    console.error(e)
                }

                if(stat){
                    if(stat.isDirectory()){
                        await fs.promises.rm(lpath, {recursive: true})
                    }
                    else{
                        await fs.promises.unlink(lpath)
                    }
                }
            }
        }

        return ignores
    }

    async readFileContent(path: string, compress = false){
        let bytes = await fs.promises.readFile(path)
        if(bytes.length == 0) return null 

        if(compress){
            bytes = Zlib.brotliCompressSync(bytes, {
                params: {
                    [Zlib.constants.BROTLI_PARAM_MODE]: Zlib.constants.BROTLI_MODE_GENERIC,
                    [Zlib.constants.BROTLI_PARAM_QUALITY]: 4
                }
            })
        }
        return bytes
    }


    async createFolders(path: string, response: SyncResponse){
        for(let folder of response.folders){
            let lpath = Path.posix.join(path, folder.name)
            if(folder.type == 'create'){
                let stat = null 
                try{
                    stat = await fs.promises.lstat(lpath)
                }
                catch(e){}

                if(stat){
                    if(!stat.isDirectory()){
                        await fs.promises.unlink(lpath)
                        await fs.promises.mkdir(lpath)
                    }
                }
                else{
                    await fs.promises.mkdir(lpath)
                }
                
            }


            if(folder.type == 'delete')
                if(fs.existsSync(lpath)) await fs.promises.rm(lpath,{recursive: true})
        }
    }


    async syncFolder(config: SyncFolderConfig, prefix = '', response: SyncResponse = null){
        let path = config.path
        let stats = config.stats 
        if(!response){
            response = {
                folders: [],
                files: [],
                ignored: []
            }
        }


        //  GET ALL FILE LIST 
        let fileData = {}

        
        let getMatchers = async (path, prefix) => {
            let minimatchs = []
            let ksyncIgnore = Path.join(path, ".ksyncignore")
            if(fs.existsSync(ksyncIgnore)){
                let content = await fs.promises.readFile(ksyncIgnore, "utf8")
                
                let lines = content.split(/\r|\n/g).map((a)=> a?.trim()).filter(Boolean)            
                
                for(let line of lines){
                    //console.info("Line>",line)
                    minimatchs.push(new minimatch.Minimatch(line, {}))
                }
            }

            if(!prefix){
                minimatchs.push(new minimatch.Minimatch("**/.sync-data", {}))
                minimatchs.push(new minimatch.Minimatch("**/.~*", {}))
                minimatchs.push(new minimatch.Minimatch(".Trash-*", {}))
            }
            return minimatchs
        }



        let checked = {}
        let fileList = async (path: string, prefix = '', parentMatchers = Array<any>()) =>{
            let matchers = await getMatchers(path, prefix)

            /*
            if(matchers.length || parentMatchers.length){
                await this.consoleLog("Matchers", prefix, matchers.length, parentMatchers.length)
            }
            */

            let files = await fs.promises.readdir(path)
            files = files.filter((a) => (a != ".") && (a != ".."))

            let names = [], fnames = [], ignored = []
            let data = fileData, folders = []
            for(let file of files){
                let ufile = Path.posix.join(path, file)
                let stat = await fs.promises.lstat(ufile)
                let name= Path.posix.join(prefix, file)
                
                fnames.push(file)

                data[name] = {
                    path: ufile,
                    name,
                    stat 
                }
            }

            for(let man of matchers){
                let todel = fnames.filter((a) => man.match(a))
                //man.match(fnames)
                if(todel.length){
                    fnames = fnames.filter((a) => todel.indexOf(a) < 0)
                    ignored.push(... todel.map((a) => Path.posix.join(prefix, a)))
                }
            }


            

            if(parentMatchers?.length){
                let todel = []
                for(let fname of fnames){                    
                    //if(fname == "node_modules"){
                        for(let mat of parentMatchers){
                            let comparename = Path.posix.join(mat.paths.join("/"), fname)        
                            if(mat.man.match(comparename)){
                                //await this.consoleLog("Match found:", comparename, fname)
                                todel.push(fname)
                                break 
                            }
                        }
                        //process.exit(1)
                    //}
                }

                if(todel.length){
                    fnames = fnames.filter((a) => todel.indexOf(a) < 0)
                    ignored.push(... todel.map((a) => Path.posix.join(prefix, a)))
                }
            }

            // SPECIAL DIRECTORY
            fnames = fnames.filter((a) => a != ".sync-data")
            names.push(...fnames.map((a) => Path.posix.join(prefix, a)))

            

            let lnames = [].concat(fnames)
            for(let fname of lnames){
                let name = Path.posix.join(prefix, fname)
                let ufile = Path.posix.join(path, fname)
                let item = data[name]
                if(item.stat.isDirectory()){
                    

                    let parentMatchers1 = matchers.map(function(man){
                        return {
                            man,
                            paths: [fname]
                        }
                    })

                    let parentMatchers2 = parentMatchers.map(function(mat){
                        return {
                            man: mat.man,
                            paths: [...mat.paths, fname]
                        }
                    })
                    parentMatchers2.push(...parentMatchers1)
                    


                    let res = await fileList(ufile, name, parentMatchers2)
                    //let newfnames = res.fnames.map((a) => Path.posix.join(fname, a))
                    //let newnames = res.names.map((a) => Path.posix.join(prefix, fname, a))
                    ignored.push(...res.ignored)
                    names.push(...res.names)
                }
            }

            /*
            for(let man of matchers){
                let todel = fnames.filter((a) => man.match(a))
                if(todel.length){
                    fnames = fnames.filter((a) => todel.indexOf(a) < 0)
                    ignored.push(todel.map((a) => Path.posix.join(prefix, a)))
                }
            }
            */
            

            return {
                names, 
                fnames,
                ignored
            }
        }

        let res = await fileList(path, prefix)        
        for(let name of res.names){
            let ufile = Path.posix.join(path, name)
            let stat = fileData[name].stat
            let fstat = stats[name]

            if(stat.isDirectory()){
                let fstat = stats[name]
                if(!fstat){
                    response.folders.push({
                        name,
                        type: 'create',
                        mtimeMs: Math.round(stat.mtimeMs),
                        atimeMs: Math.round(stat.atimeMs),
                        directory: true
                    })
                }
            }
            else if(stat.isFile() || stat.isSymbolicLink()){
                let link = ''
                if(stat.isSymbolicLink()){
                    link = await fs.promises.readlink(ufile)
                    if(Path.isAbsolute(link)){
                        
                        let link2  = Path.relative(Path.dirname(ufile), link)
                        if(Os.platform() == "win32"){
                            link2 = link2.replace(/\\/g, '/')
                        }
                        console.info("> Getting symlink as relative path:", link, "Result:", link2)
                        link = link2
                        //console.info("Converted:", link)
                    }
                }
                let isChanged = false 
                if(stat.isSymbolicLink()){
                    isChanged = fstat?.link != link
                }
                else{
                    isChanged = fstat?.mtimeMs != Math.round(stat.mtimeMs)
                    if(Os.platform() == "win32"){
						if(fstat?.link && (stat.size < 300)){
							let content = await fs.promises.readFile(ufile,'utf8')
							let lines = content.split("\n")
							if(lines[0].trim() == "[SYMLNK]"){
								link = lines[1].trim()
								isChanged = fstat?.link != link
							}
						}
					}
                }

                if(isChanged){
                    if((config.maxsize !== undefined) && (stat.size > config.maxsize)){
                        response.ignored.push({
                            name,
                            reason: "MAX_SIZE_EXCEEDED"
                        })
                    }
                    else{
                        //console.info("> File", name, stat.mtimeMs, fstat?.mtimeMs)
                        response.files.push({
                            name,
                            type: 'create',
                            mtimeMs: Math.round(stat.mtimeMs),
                            atimeMs: Math.round(stat.atimeMs),
                            mode: stat.mode,
                            size: stat.size,
                            link
                        })
                    }
                }
            }   
        }

        let filesByName = {}
        for(let name of res.names){
            filesByName[name] = true 
        }
        
        let folderdel = []
        for(let name in stats){
            let value = stats[name]
            if(!filesByName[name]){                
                if(value.directory){
                    folderdel.push({
                        name,
                        type: "delete",
                        directory:  true
                    })
                }
                else{
                    response.files.push({
                        name,
                        type: 'delete'
                    })
                }                    
            }
        }

        folderdel.sort((a,b) => b.name.split("/").length - a.name.split("/").length)
        response.files.push(...folderdel)
        response.ignored .push(...res.ignored.map(function(name){
            return {
                name,
                reason: "CONFIGURED"
            }
        }))

        return response
    }    

    
    stop(){
    }

    destroy(){
        this.stop()
    }
    
}

export class Program{

    static async execute(args: Array<string>){
        let cli = parse(args)

        if(cli.params.src && cli.params.dest){
            let rp = new FileSync()
            rp.params = cli.params
            rp.paramsArray = cli.paramsArray
            await rp.connect(cli.params.address, cli.params.password)
            await rp.prepare()
            

            
            while(true){
                if(cli.params.from == "remote"){
                    await rp.remote.bidirectionalSync(cli.params.src, cli.params.dest)
                }
                else{
                    await rp.bidirectionalSync(cli.params.src, cli.params.dest)
                }
                if(cli.params.interval){
                    console.info("> Waiting next interval")
                    await async.sleep(Number(cli.params.interval) * 1000)
                }
                else{
                    break
                }
            }

        }
            
    }

    static async main(args?: Array<string>){

        try{
            
            await this.execute(args)
            process.exit(0)

        }catch(e){
            console.error("Failed executing:", e)
        }
        finally{
            process.exit(1)
        }
    }
}
