## Waba SQLiteBot

Waba SQLiteBot is a daemon script that reads whatsapp messages from Waba Service and writes to SQLite database. 


### Install 

1. Install [kwruntime/core](https://github.com/kwruntime/core/blob/main/INSTALL.md)

2. Execute on terminal (or cmd)

```bash
kwrun --name=waba-sqlitebot --install=gitlab://jamesxt94/codes@3bb74cbc/waba/sqlitebot.ts
``` 

### Usage

```bash 
waba-sqlitebot --key=WABA_KEY --db=/path/to/sqlite.db
``` 

Automatically restart on failure:

```bash
kwrun --name=kmux --install=gitlab://jamesxt94/tmux@d0e0b805/src/mesha.ts
kmux --server --background

# this execute process in background, and automatically restart if fails
kmux --name=sqlitebot --start=waba-sqlitebot --arg=--key=WABA_KEY --arg=--db=/path/to/sqlite.db

# if you want stop the process:
kmux --name=sqlite --delete
```

In your software, you can periodically check for new messages: 

```sql
-- send the last id readed or 0 for initial
select * from message where body != '' and fromMe = 0 and id > ? order by id
``` 

### Structure of table message

Here example of data:

```json
{
	"id": 1,
	"uid": "61ff4db198d18dd39a787efd",
	"chatId": "593XXXXXXXXX@c.us",
	"body": "Mensaje de prueba",
	"filehash": null,
	"filename": null,
	"fromId": "593XXXXXXXXX@c.us",
	"fromMe": 0,
	"message_id": "false_593961483823@c.us_2EB920AF9D50B90AEC0637CA588D9B38",
	"isForwarded": 0,
	"isGroupMsg": 0,
	"isMMS": 0,
	"isMedia": 0,
	"isNotification": 0,
	"isPSA": 0,
	"mimetype": null,
	"notifyName": null,
	"self": "in",
	"sender": null,
	"time": 1644121517,
	"timestamp": 1644121517,
	"receiver": "57XXXXXXXXX@c.us",
	"type": "chat",	
	"uploadhash": null
}
``` 