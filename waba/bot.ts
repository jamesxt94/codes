/**
 * jsonbot  
 * Read messages from whatsapp using waba service
 * and write to json files in folder
 * Michael James (2022-02-05)
 * developer@kodhe.com
 */


import {Client} from 'https://gitlab.com/jamesxt94/mesha/-/raw/a5071b83/Messaging.ts'
import { RPC, ShareSymbol } from 'https://gitlab.com/jamesxt94/mesha/-/raw/a5071b83/RPC.ts'


import Path from 'path'
import fs from 'gh+/kwruntime/std@1.1.4/fs/mod.ts'


import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"
import {AsyncEventEmitter} from "gh+/kwruntime/std@1.1.4/async/events.ts" 
import mime from 'npm://mime-types@2.1.34'

 
 /*
 import "npm://terminal-image@1.2.1"
 import terminalImage from 'terminal-image'
 */
 
export interface FileOptions{
	mimeType?: string
	voice?: boolean
	gif?: boolean
	filename?: string
	caption?: string
}


export class Bot extends AsyncEventEmitter{

	static client: Client
	static rpc: RPC

	static #wabaServer: any

	instance: any
	id: string
	key: string
	query: {[key:string] : any} = {}


	constructor(key:string, id?: string){
		super()
		this.key = key
		this.id = id || key
	}

	static async connectMesha(address: string, password: string){
		let client  = this.client = await Client.connect(address)
		let rpc = this.rpc = new RPC()
		rpc.channel = client
		rpc.init()
		let start = await rpc.defaultScope.remote("GetObject")
		client.on("close", ()=>{
			console.info("Closing client ...")
			process.exit(1)
		})
		return  await start.invoke(password)
	}


	async connect(address: string){
		Bot.#wabaServer = await Bot.connectMesha(address, "357cc546b6663e7eecf7735aa72092e3")

		// start reading messages
		this.instance = await Bot.#wabaServer.getInstance(this.key)
		if(!this.instance){
			throw Exception.create("Instance not active").putCode("INSTANCE_CLOSED")
		}
		console.info("> Connected to waba")
	}

	async connectAndStart(address: string){
		await this.connect(address)
		await this.begin()
	}

	async begin(){
		let items = await this.instance.queryDb({
			tablename: "bot_data",
			query: {
				uid: this.id
			},
			limit: 1
		})

		if(!items[0]){
			//console.info("> Querying last message")
			items = await this.instance.queryDb({
				tablename: "message",
				query: {},
				sort: {
					_id: -1
				},
				limit: 1
			})
			if(items[0]){
				this.query._id = {
					$gt: items[0]._id
				}
			}
		}
		else{
			this.query._id = {
				$gt: items[0].message_id
			}
		}
		console.info("> Query:", this.query)
		setImmediate(this.start.bind(this))
		return true
	}


	async getChat(chatId: string){
		let rows = await this.instance.queryDb({
			tablename: "chat",
			query: {
				uid: chatId
			}
		})
		return rows[0]
	}

	async getSender(chatId: string){
		let rows = await this.instance.queryDb({
			tablename: "sender",
			query: {
				uid: chatId
			}
		})
		return rows[0]
	}

	async sendText(chatId: string, text: string, params: any = null){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,
			body: text,
			chatId,
			params,
			created: Date.now(),
			type: "sendMessage"
		})
	}

	async removeParticipant(chatId: string, author:string){
		let uid = await this.instance.generateUid()
		let result = await this.instance.registerInDb("actions", uid, {
			uid,
			author,
			chatId,
			updated: Date.now(),
			type: "removeParticipant"
		})
		return {
			result,
			uid
		}
	}

	async waitAction(uid: string, timeout = 120000){

		if(!uid) return 


		let actions = await this.instance.queryDb({
			tablename: 'actions',
			query: {
				uid
			},
			limit: 1
		})
		
		if(actions[0]){
			let action = actions[0]
			if(action.finished){
				if(action.error){
					let e = Exception.create(action.error.message || action.error).putCode(action.error.code)
					e.stack = action.error.stack 
					throw e 
				}
				return action.result
			}
			else{
				if(timeout <= 0){
					throw Exception.create("Timeout waiting action completion").putCode("TIMEOUT")
				}
				await async.sleep(2000)
				return await this.waitAction(uid, timeout - 2000)
			}
		}
	}


	

	async deleteMessage(chatId: string, msgId:string){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,
			msgId,
			chatId,
			updated: Date.now(),
			type: "deleteMessage"
		})
	}

	async sendButtons(chatId: string, title: string, buttons: any[], subtitle?: string ){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,
			buttons,
			title,
			subtitle,
			chatId,
			updated: Date.now(),
			type: "sendMessage"
		})
	}


	async sendListMenu(chatId: string, title: string, subtitle: string, description: string, buttonText: string, menu: any[] ){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,

			title,
			subtitle,
			description,
			buttonText,
			menu,
			updated: Date.now(),
			chatId,
			type: "sendListMenu"
		})
	}

	async sendContact(chatId: string, contactIds: string[], name?: string){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,
			contacts: contactIds,
			name,
			chatId,
			updated: Date.now(),
			type: "sendContact"
		})
	}

	async sendLocation(chatId: string, latitude: number, longitude: number, title?: string){
		let uid = await this.instance.generateUid()
		return await this.instance.registerInDb("actions", uid, {
			uid,
			latitude,
			longitude,
			title,
			updated: Date.now(),
			chatId,
			type: "sendLocation"
		})
	}

	async sendBuffer(chatId: string, bytes:Buffer, options: FileOptions = {}){
		let uid = await this.instance.generateUid()
		if(!options.mimeType){
			options.mimeType = mime.lookup(options.filename) || "application/octect-stream"
		}

		return await this.instance.registerInDb("actions", uid, {
			uid,
			caption:options.caption,
			filename: options.filename,
			base64: "data:" + options.mimeType + ";base64," + bytes.toString('base64'),
			chatId,
			updated: Date.now(),
			gif: Boolean(options.gif),
			voice: Boolean(options.voice),
			type: "sendMessage"
		})

	}

	async sendFile(chatId: string, path: string, options: FileOptions = {}){
		if(!options.filename){
			options.filename = Path.basename(path)
		}
		let bytes = await fs.readFileAsync(path)

		return await this.sendBuffer(chatId, bytes, options)
	}


	async start(){

		let enumerator = await this.instance.getEnumerator(["varchange"])
		let getChanges = async ()=>{
			while(true){
				if(await this.instance.destroyed)
					break


				let event = await enumerator.next()
				if(event?.value){
					let change = event.value.data
					this.emit(change.id, change.value)
				}
			}
		}
		getChanges()
		let status = await this.instance.getData("status")
		if(status){
			this.emit("status", status)
		}

		status = await this.instance.getData("stream")
		if(status){
			this.emit("stream", status)
		}


		while(true){
			try{

				if(await this.instance.destroyed)
					break


				let messages = await this.instance.queryDb({
					tablename: 'message',
					query: this.query,
					sort: {
						_id:1
					}
				})


				if(messages.length){
					for(let message of messages){
						this.emit("message", message)
					}
					let lmessage = messages[messages.length - 1]

					// update last reader
					this.query._id = {
						$gt: lmessage._id
					}

					await this.instance.registerInDb("bot_data", this.id, {
						uid: this.id,
						message_id: lmessage._id
					})

				}
			}catch(e){
				if(e.code == "RPA_DESTROYED" || e.code == "RPC_DESTROYED"){
					break
				}
				this.emit("error", e)
			}

			await async.sleep(500)
		}

		// restart
		this.emit("destroyed")
	}

}

export interface ChatData{
	started: number
	time?: number
	current: string
	chatId: string
	lastSent: number
	props?: any
	data?: Map<string, any>
}

export interface Event{
	continue?: boolean
}