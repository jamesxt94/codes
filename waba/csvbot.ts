/**
 * csvbot  
 * Read messages from whatsapp using waba service
 * and write to csv files in folder
 * Michael James (2022-02-05)
 * developer@kodhe.com
 */

 import fs from 'fs'
 import {Bot, ChatData} from './bot.ts'
 import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
 import Path from 'path'
 import {parse} from 'gitlab://jamesxt94/codes@e72b0bd/cli-params.ts'
 


export class BotManager{
	#key: string
	#bot: Bot
	#botData = new Map<string, ChatData>()    
	//#commands = new Map<string, any>()
	#folder: string 

	constructor(key, folder: string){
		this.#key = key
		this.#bot = new Bot(this.#key)
		this.#bot.on("error", (e)=> console.error(e.message))
		this.#folder = folder

		if(!fs.existsSync(folder))
			fs.mkdirSync(folder)
	}

	get bot(){
		return this.#bot
	}


	async start(){
		let bot = this.#bot
		let connect = async ()=>{
			while(true){
				try{
					await bot.connect("tcp://wabav5-service.kodhe.work:42002")
					break
				}
				catch(e){
					console.error("> Failed connect.", e.message)
				}
				await async.sleep(5000)
			}
		}
		let iterator = this.#bot.getIterator(["status", "screenshot", "message", "destroyed"])
		await connect()


		for await (let event of iterator){
			if(event.type == "status"){
				console.info("Status:", event.data)
			}
			else if(event.type == "destroyed"){
				console.info("Instance closed. Reconnecting...")
				await connect()
			}
			else if(event.type == "message"){
				let message = event.data

				if(!message.fromMe){
					let from = message.from
					try{
						if(typeof from == "string"){
							await this.#processMessage(message)
						}
					}
					catch(e){
						console.error("Failed processing message:", e.message)
					}
				}
				else{
					// update wabatime
					let data = this.#getData(message.to._serialized || message.to)
					data.data.set("wabamain.time", Date.now())
				}

			}
		}
	}

	#getData(chatId: any): ChatData{
		if(typeof chatId == "object"){
			chatId= chatId._serialized
		}

		let data = this.#botData.get(chatId)
		if(!data){
			data = {
				started: Date.now(),
				current: '',
				chatId,
				lastSent: 0,
				data: new Map<string, any>(),
				stream: null
			}

			// file for save data 
			let file = Path.join(this.#folder, chatId + ".csv")
			data.stream = fs.createWriteStream(file, {
				flags: 'a'
			})
			this.#botData.set(chatId, data)
		}
		return data
	}

	async #processMessage(message){
		
		let data = this.#getData(message.from)
		// save to file
		console.info("Message received:", message.from, message.id)
		if(message.body){
			let arr = [
				message._id.$oid,
				data.chatId,
				message.t,
				message.body
			]
			let str = csvString.stringify(arr)
			data.stream.write(str)
		}

	}


}


export class Program{
	static async main(){
		let cli = parse()
		let folder = Path.resolve(cli.params.folder || '.')
		let bot = new BotManager(cli.params.key, folder)
	    await bot.start()
	}
}