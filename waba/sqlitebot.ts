/**
 * sqlitebot  
 * Read messages from whatsapp using waba service
 * and write to SQLite database
 * parameters:
 * --db=/path/to/db --key=WABA_KEY
 * 
 * How read from messages from external application?
 * select * from message where body != '' and fromMe = 0 and id > 0 order by id
 * 
 * Michael James (2022-02-05)
 * developer@kodhe.com
 */


import fs from 'fs'
import {Bot, ChatData} from './bot.ts'
import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import Path from 'path'
import {parse} from 'gitlab://jamesxt94/codes@e72b0bd/cli-params.ts'
import { Writable } from 'stream'

import sqlite3 from 'npm://sqlite3@5.0.2'



export class BotManager{
	#key: string
	#bot: Bot
	#botData = new Map<string, ChatData>()
	dbfile: string
	db: sqlite3.Database
	stream: Writable
	#options: any 
	constructor(options: any){
		this.#options = options
		this.#key = options.key
		this.#bot = new Bot(this.#key)
		this.#bot.on("error", (e)=> console.error(e.message))
		
	}

	

	get bot(){
		return this.#bot
	}


	async start(){

		// start the db
		let def = new async.Deferred<any>()
		let asyncCallback = (e, data) => e ? def.reject(e) : def.resolve(data)

		this.db = new sqlite3.Database(this.dbfile)

		let query = `
		CREATE TABLE IF NOT EXISTS config (
			name TEXT,
			value TEXT
		);
		`
		this.db.exec(query, (e, data) => e ? def.reject(e) : def.resolve(data))
		await def.promise 


		query = `
		CREATE TABLE IF NOT EXISTS chat (
			id INTEGER PRIMARY KEY	AUTOINCREMENT,
			key TEXT,
			updated INTEGER,
			uid TEXT UNIQUE,
			isGroup INTEGER,
			isOnline INTEGER,
			kind TEXT,
			notSpam INTEGER,
			time INTEGER,
			isReadOnly INTEGER,
			name TEXT,
			pin INTEGER,

			formattedName TEXT,
			isBusiness INTEGER,
			isEnterprise INTEGER,
			isMe INTEGER,
			isMyContact INTEGER,
			isPSA INTEGER,
			isUser INTEGER,
			isWAContact INTEGER,
			pushname TEXT
		);
		`
		this.db.exec(query, (e, data) => e ? def.reject(e) : def.resolve(data))
		await def.promise 

		
		this.db.get('SELECT value FROM config WHERE name = ?', 'version', asyncCallback)
		let result = await def.promise
		if(!result){
			// start database creation			
			query = `
			CREATE TABLE IF NOT EXISTS message (
				id INTEGER PRIMARY KEY	AUTOINCREMENT,
				key TEXT,
				updated INTEGER,
				uid TEXT UNIQUE,
				chatId TEXT,
				body TEXT,
				filehash TEXT,
				filename TEXT,
				fromId TEXT,
				fromMe INTEGER,
				message_id TEXT,
				isForwarded INTEGER,
				isGroupMsg INTEGER,
				isMMS INTEGER,
				isMedia INTEGER,
				isNotification INTEGER,
				isPSA INTEGER,
				mimetype TEXT,
				notifyName TEXT,
				self TEXT,
				sender TEXT,
				time INTEGER,
				timestamp INTEGER,
				receiver TEXT,
				type TEXT,
				uploadhash TEXT
			);
			`
			def = new async.Deferred<any>()
			this.db.exec(query, asyncCallback)
			await def.promise 

			def = new async.Deferred<any>()
			this.db.exec("INSERT INTO config (name,value) VALUES ('version', '1.0')", asyncCallback)
			await def.promise
			result = {value: '1.0'}
		}

		def = new async.Deferred<any>()
		this.db.exec("PRAGMA journal_mode=WAL;")
		await def.promise
	
	

		let bot = this.#bot
		let connect = async ()=>{
			while(true){
				try{
					await bot.connect("tcp://wabav5-service.kodhe.work:42002")
					break
				}
				catch(e){
					console.error("> Failed connect.", e.message)
				}
				await async.sleep(5000)
			}
		}
		let iterator = this.#bot.getIterator(["status", "screenshot", "message", "destroyed"])
		await connect()


		for await (let event of iterator){
			if(event.type == "status"){
				console.info("Status:", event.data)
			}
			else if(event.type == "destroyed"){
				console.info("Instance closed. Reconnecting...")
				await connect()
			}
			else if(event.type == "message"){
				let message = event.data

				//if(!message.fromMe){
					let from = message.from
					try{
						if(typeof from == "string"){
							await this.#processMessage(message)
						}
					}
					catch(e){
						console.error("Failed processing message:", e.message)
					}
				/*}
				else{
					// update wabatime
					let data = this.#getData(message.to._serialized || message.to)
					data.data.set("wabamain.time", Date.now())

					this.#processMessage(message)
				}*/

			}
		}
	}

	#getData(chatId: any): ChatData{
		if(typeof chatId == "object"){
			chatId= chatId._serialized
		}

		let data = this.#botData.get(chatId)
		if(!data){
			data = {
				started: Date.now(),
				time: Date.now(),
				current: '',
				chatId,
				lastSent: 0,
				data: new Map<string, any>()
			}
			this.#botData.set(chatId, data)
		}
		return data
	}

	async #processMessage(message){
		console.info("Message received:", message.from, message.id)

		let func = async ()=>{
			let data = this.#getData(message.from)
			// save to file			
			if(this.#options.fromMe === undefined){
				if(message.fromMe) return 
			}

			let def = new async.Deferred<any>()
			let asyncCallback = (e, data) => e ? def.reject(e) : def.resolve(data)
			this.db.run(`INSERT INTO message (key, updated, uid, chatId, body, filehash, filename, fromId, fromMe, message_id, isForwarded, isGroupMsg,
				isMMS, isMedia, isNotification, isPSA, mimetype, notifyName, self, sender, time, timestamp, receiver, type, uploadhash)
				VALUES (${("?????????????????????????").split("").join(", ")});`, 
				this.bot.key, Date.now(), message._id.$oid, message.from, message.body, message.filehash, message.filename, message.from, message.fromMe,
				message.id, message.isForwarded, message.isGroupMsg, message. isMMS, message.isMedia, message.isNotification,
				message.isPSA, message.mimetype, message.notifyName, message.self, message.sender, message.t, message.timestamp,
				message.to, message.type, message.uploadhash, asyncCallback)
			await def.promise

			let chat = data.data.get("chatInfo")
			if(!chat || (Date.now() - data.time > 120000)){
				try{
					// update chat 
					let chat = await this.bot.getChat(message.from)
					let sender = await this.bot.getSender(message.from) || {}
					
					def = new async.Deferred<any>()
					let updated = Date.now()
					this.db.run(`INSERT INTO chat (key, updated, uid, isGroup, isOnline, kind, notSpam, time, isReadOnly, name, pin,
						formattedName, isBusiness, isEnterprise, isMe, isMyContact, isPSA, isUser, isWAContact, pushname)
						VALUES (${("????????????????????").split("").join(", ")}) ON CONFLICT(uid) DO
						UPDATE SET 
							updated = ?,  notSpam = ?,  time = ?,  isReadOnly = ?,
							name = ?,  pin = ?,  formattedName = ?,  isBusiness = ?,
							isEnterprise = ?,  isMyContact = ?,  isPSA = ?, pushname = ?;`, 
						this.bot.key, updated, message.from, chat.isGroup, chat.isOnline, chat.kind, chat.notSpam, chat.t, chat.isReadOnly, chat.name, chat.pin,
						sender.formattedName, sender.isBusiness, sender.isEnterprise, sender.isMe, sender.isMyContact, sender.isPSA, sender.isUser,
						sender.isWAContact, sender.pushname,
						updated, chat.notSpam, chat.t, chat.isReadOnly, chat.name, chat.pin, // Here start update data 
						sender.formattedName, sender.isBusiness, sender.isEnterprise, sender.isMyContact, sender.isPSA, sender.pushname,
						asyncCallback)  
					await def.promise

					data.data.set("chatInfo",{
						chat,
						sender
					})
					data.time = updated
				}catch(e){
					console.error("Failed getting/updating chat info:", e.message)
				}
			}
		}

		// retry 
		let retries = 20, time = 50
		while(true){
			try{
				await func()
				break 
			}catch(e){
				if(retries == 0) throw e 
				retries --
				if(e.message.indexOf("locked") >= 0){
					// wait a little time??
					time = Math.max(time, 1000)
					console.info("> [WARNING] Database locked, delaying "+time+"ms to retry")
					await async.sleep(time)
					time*= 2
					
				}
			}
		}

	}


}


export class Program{
	static async main(){
		try{
			let cli = parse()

			if(!cli.params.db || !cli.params.key){
				console.error("Please specify --db and --key parameters")
				process.exit(1)
			}

			let bot = new BotManager(cli.params)
			if(cli.params.db){
				cli.params.db = Path.resolve(cli.params.db)
			}
			bot.dbfile = cli.params.db
			await bot.start()
		}catch(e){
			console.error("Failed start:", e.message)
		}
	}
}