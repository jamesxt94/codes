/**
 * stdoutbot
 * Read messages from waba and write to stdout
 * 
 * You can read from external application
 * 
 * Michael James (2022-02-05)
 * developer@kodhe.com
 */


import fs from 'fs'
import {Bot, ChatData} from './bot.ts'
import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import Path from 'path'
import {parse} from 'gitlab://jamesxt94/codes@e72b0bd/cli-params.ts'
import { Writable } from 'stream'

import sqlite3 from 'npm://sqlite3@5.0.2'



export class BotManager{
	#key: string
	#bot: Bot
	#botData = new Map<string, ChatData>()
	dbfile: string
	stream: Writable
	#options: any 
	constructor(options: any){
		this.#options = options
		this.#key = options.key

		this.#bot = new Bot(this.#key)
		this.#bot.on("error", (e)=> {
			console.info("#WABA-JSON", JSON.stringify({
				type:'error',
				data: {
					message: e.message,
					code: e.code
				}
			}))
		})
		
	}

	

	get bot(){
		return this.#bot
	}


	async start(){
		let bot = this.#bot
		let connect = async ()=>{
			while(true){
				try{
					await bot.connect("tcp://wabav5-service.kodhe.work:42002")
					break
				}
				catch(e){
					console.error("> Failed connect.", e.message)
					
				}
				await async.sleep(5000)
			}
		}
		let iterator = this.#bot.getIterator(["status", "screenshot", "message", "destroyed"])
		await connect()


		for await (let event of iterator){
			if(event.type == "status"){
				console.info("Status:", event.data)
				console.info("#WABA-JSON", JSON.stringify(event))
			}
			else if(event.type == "destroyed"){
				console.info("Instance closed. Reconnecting...")
				console.info("#WABA-JSON", JSON.stringify(event))
				await connect()
			}
			else if(event.type == "message"){
				let message = event.data

				let from = message.from
				try{
					if(typeof from == "string"){
						if(message.fromMe){
							if(this.#options["include-my"] !== undefined){
								console.info("#WABA-JSON", JSON.stringify(event))			
							}
						}
						else{
							console.info("#WABA-JSON", JSON.stringify(event))
						}
					}
				}
				catch(e){
					console.error("Failed processing message:", e.message)
				}
			}
		}
	}

	/*
	#getData(chatId: any): ChatData{
		if(typeof chatId == "object"){
			chatId= chatId._serialized
		}

		let data = this.#botData.get(chatId)
		if(!data){
			data = {
				started: Date.now(),
				current: '',
				chatId,
				lastSent: 0,
				data: new Map<string, any>()
			}
			this.#botData.set(chatId, data)
		}
		return data
	}*/


}


export class Program{
	static async main(){
		try{
			let cli = parse()

			if(!cli.params.key){
				console.error("Please specify --key parameters")
				process.exit(1)
			}

			let bot = new BotManager(cli.params)
			await bot.start()
		}catch(e){
			console.error("Failed start:", e.message)
		}
	}
}