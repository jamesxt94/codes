import {Caddy as CaddyType} from './caddy/server.ts'
import Child from 'child_process'
import {kawix} from 'gh+/kwruntime/core@2009992/src/kwruntime.ts'


export var Caddy = CaddyType
export class Program{
	static async main(){
		try{
			const res = await CaddyType.download()
			Child.spawn(res.bin, kawix.appArguments.slice(1), {
				stdio:'inherit'
			})	
		}catch(e){
			console.error("Failed executing:", e)
			process.exit(1)
		}
	}
}
