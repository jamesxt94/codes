// James 2022-03-11
// automatically download caddy and start
import tar from "npm://tar@6.1.12"
import zip from "npm://adm-zip@0.5.9"


import fetch from 'npm://node-fetch@3.2.0'
import Os from 'os'
import {Exception} from "gh+/kwruntime/std@1.1.14/util/exception.ts"
import * as async from "gh+/kwruntime/std@1.1.14/util/async.ts"
import Path from 'path'
import {pipeline} from 'stream'
import {promisify} from 'util'
import Child from 'child_process'
import fs from 'fs'
import { CaddyConfig } from './types.ts'

export interface Cmd{
	bin?: string
	args?: Array<string>
	options?: {[key:string]: any}
	stdin?: string 
}


export class Caddy{
	static #download : any 
	#id: string 
	#config: CaddyConfig
	constructor(id: string = 'default'){
		this.#id = id
		this.#config = {}
	}

	get config(){
		return this.#config
	}

	async getCmd(){
		return await Caddy.getCmdJSONWithId(this.#id, this.config)
	}

	async update(){
		await this.getCmd()
	}

	static async getCmd(id: string, config: string){

		let download= await Caddy.download()
		let configFolder = Path.join(download.userfolder, "config")
		if(!fs.existsSync(configFolder)) await fs.promises.mkdir(configFolder)

		configFolder = Path.join(download.userfolder, id)
		if(!fs.existsSync(configFolder)) await fs.promises.mkdir(configFolder)


		let configFile = Path.join(configFolder, "Caddyfile")
		await fs.promises.writeFile(configFile, config)

		return {
			bin: download.bin,
			stdin: config,
			args: ["run"],
			options: {
				cwd: configFolder
			}
		}
	}

	static async getCmdJSON(config: string){
		let download= await Caddy.download()
		
		return {
			bin: download.bin,
			stdin: Buffer.from(config),
			args: ["run", "--config", "-"],
			options: {}
		}
	}

	static async getCmdJSONWithId(id: string, config: {[key:string]: any}){
		let download= await Caddy.download()

		let configFolder = Path.join(download.userfolder, "config")
		if(!fs.existsSync(configFolder)) await fs.promises.mkdir(configFolder)
		let out = Path.join(configFolder, id + ".json")
		await fs.promises.writeFile(out, JSON.stringify(config, null, '\t'))
		return {
			bin: download.bin,
			args: ["run", "--config", out, "--watch"],
			options: {}
		}
	}


	start(cmd: Cmd, options?){
		let p =  Child.spawn(cmd.bin, cmd.args,Object.assign({
			stdio:['pipe','inherit','inherit']
		}, cmd.options, options || {}))
		if(cmd.stdin){
			p.stdin.write(cmd.stdin + "\n")
		}
		return p
	}


	static async download(){
		if(this.#download) return this.#download
		let res = await fetch("https://api.github.com/repos/caddyserver/caddy/releases")
		let json = await res.json()
		
		let platform = Os.platform()
		let archConversions = {
			"x64": "amd64",
			"x86": "386",
			"ia32": "386"
		}
		let arch = archConversions[Os.arch()]
		let regex = new RegExp(`_${platform}_${arch}\.(tar.gz|zip)$`,"i")
		let usable = null 

		json.reverse()
		for(let item of json){
			let assets = item.assets
			for(let asset of assets){
				let match = asset.name.match(regex)
				if(match){
					usable = {
						item,
						asset
					}
					break 
				}
			}
		}

		if(!usable){
			throw Exception.create("There is not relase available for your platform.").putCode("RELEASE_UNAVAILABLE")
		}
		

		// 	user data folder
		let path = Path.join(Os.homedir(),".kawi","user-data")
		if(!fs.existsSync(path)) await fs.promises.mkdir(path)
		path = Path.join(path, "caddy")
		if(!fs.existsSync(path)) await fs.promises.mkdir(path)
		let out = Path.join(path, usable.asset.name)
		let ok =  Path.join(path, usable.asset.name + ".ok.json")
		// ahora descomprimir 
		let bin = Path.join(path, "bin")
		if(!fs.existsSync(ok)){
			console.info(`Downloading ${usable.asset.name}`)

			res = await fetch(usable.asset.browser_download_url)
			const streamPipeline = promisify(pipeline)
			await streamPipeline(res.body, fs.createWriteStream(out))
			let stat = await fs.promises.stat(out)
			if(stat.size != usable.asset.size){
				throw Exception.create("Download failed. Invalid content size").putCode("CORRUPT_DOWNLOAD")
			}
			
			if(usable.asset.name.endsWith(".tar.gz")){
				if(!fs.existsSync(bin)) await fs.promises.mkdir(bin)
				//let tar = await import("npm://tar@6.1.11")
				let extractor = tar.x({
					C: bin
				})
				let reader = fs.createReadStream(out)
				let def = new async.Deferred<void>()
				reader.once("error", def.reject)
				extractor.once("error", def.reject)
				extractor.once("finish", def.resolve)
				reader.pipe(extractor)
				await def.promise
			}
			else if(usable.asset.name.endsWith(".zip")){
				if(!fs.existsSync(bin)) await fs.promises.mkdir(bin)
				//let zip = await import("npm://adm-zip@0.5.9")
				zip.extractAllTo(bin, true)
			}

			await fs.promises.writeFile(ok, Date.now().toString())
		}
		return this.#download = {
			folder:bin,
			userfolder: path,
			bin: Path.join(bin, "caddy")
		}
		

	}

}