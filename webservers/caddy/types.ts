export interface CaddyAdminConfig{
	disabled?: boolean
	listen?: string 
	enforce_origin?: boolean 
	origins?: Array<string>
	config?: {
		persist?: boolean 
	}
	identity?:{
		identifiers?: Array<string>
		issuers?: CaddyIssuance
	}
	remote?:{
		listen?: string
		access_control?: Array<CaddyAdminAccessControl>
	}
}
export interface CaddyAdminAccessControl{
	public_keys?: Array<string>
	permissions?: [{
		paths: Array<string>
		methods: Array<string>
	}]
}

export interface CaddyAppsConfig{
	http?: {
		http_port?: number
		https_port?: number
		grace_period?: number 
		servers?: {[key: string]: CaddyHttpServer}
	}
}

interface CaddyEncoderCommon{
	format: string 
	message_key?: string 
	level_key?: string 
	time_key?: string 
	name_key?: string
	caller_key?: string 
	stacktrace_key?: string 
	line_ending?: string 
	time_format?: string 
	duration_format?: string 
	level_format?: string 
}
export interface CaddyEncoderConsole extends CaddyEncoderCommon{
	format: 'console'
}
export interface CaddyEncoderJSON extends CaddyEncoderCommon{
	format: 'json'
}
export interface CaddyEncoderLogFmt extends CaddyEncoderCommon{
	format: 'logfmt'
}
export interface CaddyEncoderFilter{
	format: 'filter'
	wrap?: CaddyEncoder
	fields?: {
		delete?: {
			filter:'delete'
		},
		ip_mask?: {
			filter: 'ipmask',
			ipv4_cidr?: number 
			ipv6_cidr?: number 
		}
		replace: {
			filter: 'replace'
			value?: string 
		}
	}
}
export interface CaddyEncoderSingleField{
	format: 'single_field'
	field?: string 
	fallback?: CaddyEncoder
}
export type CaddyEncoder = CaddyEncoderConsole | CaddyEncoderJSON | CaddyEncoderLogFmt | CaddyEncoderFilter | CaddyEncoderSingleField

export interface CaddyHeadersActions{
	add?: {[key: string]: Array<string>}
	set?: {[key: string]: Array<string>}
	delete?: Array<string>
	replace?: {[key: string]: Array<CaddyRegExpSearch>}
}
export interface CaddyHeadersFullActions{
	add?: {[key: string]: Array<string>}
	set?: {[key: string]: Array<string>}
	delete?: Array<string>
	replace?: {[key: string]: Array<CaddyRegExpSearch>}
	require?: {
		status_code?: Array<number>
		headers?: {[key: string]: Array<string>}
	}
	deferred?: boolean
}
export interface CaddyHeaders{
	request?: CaddyHeadersActions
	response?: CaddyHeadersFullActions
}

export interface CaddyHttpServer{
	listen?: Array<string>
	listener_wrappers?: Array<CaddyTlsWrapper>
	read_timeout?: number 
	read_header_timeout?: number 
	write_timeout?: number
	idle_timeout?: number 
	max_header_bytes?: number 
	routes?: Array<CaddyHttpRoute>
	errors?: {
		routers?: Array<CaddyHttpRoute>
	}
	tls_connection_policies?: [{
		match?: Array<CaddyRouteMatch>
		certificate_selection?: {
			serial_number?: any 
			subject_organization?: Array<string>
			public_key_algorithm?: number
			any_tag?: Array<string>
			all_tags?: Array<string>
		}
		cipher_suites?: Array<string>,
		curves?: Array<string>,
		alpn?: Array<string>,
		protocol_min?: string 
		protocol_max?: string 
		client_authentication?: {
			trusted_ca_certs?: Array<string>
			trusted_ca_certs_pem_files?: Array<string>
			trusted_leaf_certs?: Array<string>
			mode?: string
		},
		default_sni?: string
	}]
	automatic_https?: {
		disable?: boolean
		disable_redirects?: boolean
		skip?: Array<string>
		skip_certificates?: Array<string>
		ignore_loaded_certificates?: boolean
	},
	strict_sni_host?: boolean 
	logs?: {
		default_logger_name?: "",
		logger_names?: {[key:string]: string}
		skip_hosts?: Array<string>
		skip_unmapped_hosts?: boolean
	}
	experimental_http3?: boolean
	allow_h2c?: boolean
}
export interface CaddyHttpRoute{
	group?: string 
	match?: Array<CaddyRouteMatch>,
	handle?: Array<CaddyHttpRouteHandler>,
	terminal?:  boolean
}
export interface CaddyTlsWrapper{
	tls?: {
		wrapper: 'tls'
	}
}
export type CaddyHttpRouteHandler = CaddyHttpEncode | CaddyHttpFileServer  | CaddyHttpHeaders | CaddyHttpMap | CaddyHttpMetrics | CaddyHttpPush | CaddyHttpRequestBody | CaddyHttpReverseProxy | CaddyHttpRewrite | CaddyHttpStaticResponse | CaddyHttpSubRoute | CaddyHttpVars | CaddyHttpError

export interface CaddyHttpEncode{
	handler: "encode"
	encodings?: {
		br? : {}
		gzip?: {
			level?: number
		}
		zstd?: {}
	}
	prefer?: Array<string>
	minimum_length?: number
	match?: {
		status_code?: Array<number>
		headers?: {[key:string]: Array<string>}
	}
}
export interface CaddyHttpError{
	handler: "error",
	error?: string
	status_code?: string 
}
export interface CaddyHttpFileServer{
	handler: "file_server",
	root?: string
	hide?: Array<string>
	index_names?: Array<string>
	browse?: {
		template_file?: string
	},
	canonical_uris?: boolean
	status_code?: string
	pass_thru?: boolean
	precompressed?: {
		br? : {}
		gzip?: {
			level?: number
		}
		zstd?: {}
	}
	precompressed_order?: Array<string>
}
export interface CaddyHttpHeaders extends CaddyHeaders{
	handler: "headers"
}
export interface CaddyHttpMap{
	handler: "map"
	source?: string,
	destinations?: Array<string>
	mappings?: [{
		input?: string
		input_regexp?: string
		outputs?: []
	}],
	defaults?: Array<string>
}
export interface CaddyHttpMetrics{
	handler: "metrics",
	disable_openmetrics?: boolean
}
export interface CaddyHttpPush{
	handler: "push"
	resources?: [{
		method?: string
		target?: string
	}]
	headers?: CaddyHeadersActions
}
export interface CaddyHttpRequestBody{
	handler: "request_body"
	max_size?: number
}
export interface CaddyHttpReverseProxy{
	handler: 'reverse_proxy'
	transport?: CaddyTransport
	circuit_breaker?: any
	load_balancing?: {
		selection_policy?: CaddySelectionPolicy
		try_duration?: number 
		try_interval?: number 
		retry_match?: Array<CaddyRouteMatch>
	}
	health_checks?: {
		active?: {
			path?: string 
			uri?: string 
			port?: number
			headers? : {[key: string]: Array<string>}
			interval?: number  
			timeout?: number 
			max_size?: number 
			expect_status?: number 
			expect_body?: string 
		}
		passive?: {
			fail_duration?: number 
			max_fails?: number 
			"unhealthy_request_count"?: number 
			"unhealthy_status"?: Array<number>
			"unhealthy_latency"?: number 
		}
	},
	upstreams?: [
		{
			dial?: string 
			lookup_srv?: string 
			max_requests?: number 
		}
	]
	flush_interval?: number 
	headers?: CaddyHeaders
	buffer_requests?: boolean
	buffer_responses?: boolean
	max_buffer_size?: number
	handle_response?: [{
		match?: {
			status_code?: Array<number>
			headers?: {[key: string]: Array<string>}
		},
		status_code?: string
		routes?: [{
			group?: string
			match?: [CaddyRouteMatch]
			handle?: [CaddyHttpRouteHandler]
			terminal?: boolean 
		}]
	}]
}
export interface CaddyHttpRewrite{
	handler: "rewrite",
	method?: string
	uri?: string 
	strip_path_prefix?: string
	strip_path_suffix?: string
	uri_substring?: [{
		find?: string 
		replace?: string 
		limit?: number
	}],
	path_regexp?: [{
		find?: string
		replace?: string
	}]
}
export interface CaddyHttpStaticResponse{
	handler: 'static_response'
	status_code?: string 
	headers?: {[key:string]: Array<string>}
	body?: string 
	close?: boolean
	abort?: boolean
}
export interface CaddyHttpSubRoute{
	handler: 'subroute'
	routes?: [{
		group?: string
		match?: Array<CaddyRouteMatch>
		handle?: Array<CaddyHttpRouteHandler>
		terminal?: boolean
	}],
	errors?: {
		routes?: [{
			group?: string
			match?: Array<CaddyRouteMatch>
			handle?: Array<CaddyHttpRouteHandler>
			terminal?: boolean
		}]
	}
}
type vars = {[key: string]: string}
export interface CaddyHttpVars extends vars{
	handler: 'vars'
}

export type CaddyIssuance = CaddyIssuanceAcme | CaddyIssuanceInternal | CaddyIssuanceZeroSsl
export interface CaddyIssuanceZeroSsl extends CaddyIssuanceCommon{
	module: 'zerossl'
}
export interface CaddyIssuanceInternal{
	module: 'internal'
	ca? : string
	lifetime?: number 
	sign_with_root?: boolean
}
export interface CaddyIssuanceAcme extends CaddyIssuanceCommon{
	module: 'acme'
}
export interface CaddyIssuanceCommon{
	ca? : string
	test_ca?: string
	email?: string
	account_key?: string
	external_account?: {
		key_id: string 
		mac_key: string 
	}
	acme_timeout?: number 
	challenges?: {
		http?: CaddyAcmeChallenge,
		"tls-alpn"?: CaddyAcmeChallenge,
		dns?: CaddyAcmeDnsChallenge
		bind_host?: string
	}
	trusted_roots_pem_files?: Array<string>,
	preferred_chains?: {
		smallest?: boolean 
		root_common_name?: Array<string>
		any_common_name?: Array<string>
	}
}
export interface CaddyAcmeChallenge{
	disabled?: boolean
	alternate_port: number
}
export interface CaddyAcmeDnsChallenge{
	provider?: any
	ttl?: number 
	propagation_timeout?: number 
	resolvers?: Array<string>
}

export interface CaddyLoggingConfig{
	sink?:{
		writer?: CaddyWriter
	}
	logs: {[key:string]: CaddyLog}
}
export interface CaddyLog{
	writer?: CaddyWriter
	encoder?: CaddyEncoder
	level? : string 
	sampling?: {
		interval?: number 
		first?: number
		thereafter?: number
	}
	include?: Array<string>
	exclude?: Array<string>
}
export interface CaddyWriterDiscard{
	output: "discard"
}
export interface CaddyWriterStderr{
	output: "stderr"
}
export interface CaddyWriterStdout{
	output: "stdout"
}
export interface CaddyWriterFile{
	output: "file"
	filename?: string 
	roll?: boolean
	roll_size_mb?: boolean 
	roll_gzip?: boolean
	roll_local_time?: boolean 
	roll_keep?: 0	
	roll_keep_days?: 0	
}
export interface CaddyWriterNet{
	output: "net"
	address?: string 
	dial_timeout?: number 
}
export type CaddyWriter = CaddyWriterDiscard | CaddyWriterStderr | CaddyWriterStdout | CaddyWriterFile

export interface CaddyConfig {
	admin?: CaddyAdminConfig
	logging?: CaddyLoggingConfig
	storage?: {
		file_system?:{
			module:'file_system'
			root?: string 
		}
	}
	apps?: CaddyAppsConfig
}

export interface CaddyRegExpSearch{
	search?: string
	search_regexp?: string 
	replace?: string 
}
export interface CaddyRegExp{
	name?: string
	pattern?: string 
}

export interface CaddyRouteMatch{
	expression?: string
	header?: {[key:string] : Array<string>}
	header_regexp?: {[key:string] : CaddyRegExp}
	host?: Array<string>
	method?: Array<string>
	not?: Array<CaddyRouteMatch>
	path?: Array<string>
	path_regexp?: CaddyRegExp,
	protocol?: "http" | "https" | "grpc"
	query?: {[key:string] : Array<string>}
	remote_ip?: {
		ranges?: Array<string>
		forwarded?: boolean
	}
	vars_regexp?: {[key:string] : CaddyRegExp}
	vars?: {[key:string] : string}
	file?: {
		root?: string
		try_files?: Array<string >
		try_policy?: string 
		split_path?: Array<string>
	}
}

export interface CaddySelectionPolicyCookie{
	"policy": "cookie"
	"name"?: string 
	"secret"?: string
}
export interface CaddySelectionPolicyFirst{
	"policy": "first"
}
export interface CaddySelectionPolicyIpHash{
	"policy": "ip_hash"
}
export interface CaddySelectionPolicyLeastConn{
	"policy": "least_conn"
}
export interface CaddySelectionPolicyRoundRobin{
	"policy": "round_robin"
}
export interface CaddySelectionPolicyUriHash{
	"policy": "uri_hash"
}
export interface CaddySelectionPolicyHeader{
	"policy": "header"
	field: string 
}
export interface CaddySelectionPolicyRandomChoose{
	"policy": "random_choose"
	choose?: number
}
export interface CaddySelectionPolicyRandom{
	"policy": "random"
}
export type CaddySelectionPolicy = CaddySelectionPolicyCookie | CaddySelectionPolicyFirst | CaddySelectionPolicyIpHash | CaddySelectionPolicyLeastConn | CaddySelectionPolicyRoundRobin |  CaddySelectionPolicyUriHash | CaddySelectionPolicyHeader | CaddySelectionPolicyRandomChoose | CaddySelectionPolicyRandom

export interface CaddyTransportHttp{
	protocol: 'http'
	resolver?: {
		addresses?: Array<string>
	}
	tls?: {
		root_ca_pool?: Array<string>
		root_ca_pem_files?: Array<string>
		client_certificate_file?: string
		client_certificate_key_file?: string
		client_certificate_automate?: string
		insecure_skip_verify?: boolean 
		handshake_timeout?: number 
		server_name?: string 
	}
	keep_alive?:{
		enabled?: boolean
		probe_interval?: number
		max_idle_conns?: number
		max_idle_conns_per_host?: number
		idle_timeout?: number
	}
	compression?: boolean 
	max_conns_per_host?: number
	dial_timeout?: number
	dial_fallback_delay?: number
	response_header_timeout?: number
	expect_continue_timeout?: number
	max_response_header_size?: number
	write_buffer_size?: number 
	read_buffer_size?: number
	versions?: Array<string>
}
export interface CaddyTransportFastCgi{
	protocol: 'fastcgi'
	root?: string 
	split_path?: Array<string>
	resolve_root_symlink?: boolean,
	env?: {[key:string]: string}
	dial_timeout?: number
	read_timeout?: number
	write_timeout?: number
}
export type CaddyTransport = CaddyTransportFastCgi | CaddyTransportHttp