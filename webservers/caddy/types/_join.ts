
import fs from 'fs'
import Path from 'path'

export class Program{
	static async main(){
		let files = (await fs.promises.readdir(__dirname)).filter((a) => (!a.startsWith("_")) && a.endsWith(".ts")).map((a) => Path.join(__dirname, a))
		let allcontent = []
		for(let file of files){
			let content = await fs.promises.readFile(file,"utf8")
			let lines = content.split(/\r?\n/g)
			for(let i=0;i<lines.length;i++){
				let line = lines[i] || ''
				if(line){
					if(!line.startsWith("import")){
						// from here ...
						let str = lines.slice(i).filter(Boolean).join("\n")
						str = str.replace(/\s+A\.[A-Z0-9a-z]+/g, (text) => text.replace("A.",""))
						allcontent.push(str)
						break 
					}
				}
			}
		}

		await fs.promises.writeFile(Path.join(__dirname, "..", "types.ts"), allcontent.join("\n\n"))
	}
}