import { CaddyIssuance } from "./issuance";

export interface CaddyAdminConfig{
	disabled?: boolean
	listen?: string 
	enforce_origin?: boolean 
	origins?: Array<string>
	config?: {
		persist?: boolean 
	}
	identity?:{
		identifiers?: Array<string>
		issuers?: CaddyIssuance
	}
	remote?:{
		listen?: string
		access_control?: Array<CaddyAdminAccessControl>
	}
}

export interface CaddyAdminAccessControl{
	public_keys?: Array<string>
	permissions?: [{
		paths: Array<string>
		methods: Array<string>
	}]
}