import {CaddyHttpServer} from './http'
export interface CaddyAppsConfig{
	http?: {
		http_port?: number
		https_port?: number
		grace_period?: number 
		servers?: {[key: string]: CaddyHttpServer}
	}
}