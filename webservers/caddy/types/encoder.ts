

interface CaddyEncoderCommon{
	format: string 
	message_key?: string 
	level_key?: string 
	time_key?: string 
	name_key?: string
	caller_key?: string 
	stacktrace_key?: string 
	line_ending?: string 
	time_format?: string 
	duration_format?: string 
	level_format?: string 
}


export interface CaddyEncoderConsole extends CaddyEncoderCommon{
	format: 'console'
}
export interface CaddyEncoderJSON extends CaddyEncoderCommon{
	format: 'json'
}
export interface CaddyEncoderLogFmt extends CaddyEncoderCommon{
	format: 'logfmt'
}

export interface CaddyEncoderFilter{
	format: 'filter'
	wrap?: CaddyEncoder
	fields?: {
		delete?: {
			filter:'delete'
		},
		ip_mask?: {
			filter: 'ipmask',
			ipv4_cidr?: number 
			ipv6_cidr?: number 
		}
		replace: {
			filter: 'replace'
			value?: string 
		}
	}
}

export interface CaddyEncoderSingleField{
	format: 'single_field'
	field?: string 
	fallback?: CaddyEncoder
}

export type CaddyEncoder = CaddyEncoderConsole | CaddyEncoderJSON | CaddyEncoderLogFmt | CaddyEncoderFilter | CaddyEncoderSingleField

