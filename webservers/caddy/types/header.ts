import { CaddyRegExpSearch } from "./regexp";


export interface CaddyHeadersActions{
	add?: {[key: string]: Array<string>}
	set?: {[key: string]: Array<string>}
	delete?: Array<string>
	replace?: {[key: string]: Array<CaddyRegExpSearch>}
}

export interface CaddyHeadersFullActions{
	add?: {[key: string]: Array<string>}
	set?: {[key: string]: Array<string>}
	delete?: Array<string>
	replace?: {[key: string]: Array<CaddyRegExpSearch>}
	require?: {
		status_code?: Array<number>
		headers?: {[key: string]: Array<string>}
	}
	deferred?: boolean
}

export interface CaddyHeaders{
	request?: CaddyHeadersActions
	response?: CaddyHeadersFullActions
}