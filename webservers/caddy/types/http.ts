import * as A from './http_handlers'
import { CaddyRouteMatch } from "./routeMatch";

export interface CaddyHttpServer{
	listen?: Array<string>
	listener_wrappers?: Array<CaddyTlsWrapper>
	read_timeout?: number 
	read_header_timeout?: number 
	write_timeout?: number
	idle_timeout?: number 
	max_header_bytes?: number 
	routes?: Array<CaddyHttpRoute>
	errors?: {
		routers?: Array<CaddyHttpRoute>
	}
	tls_connection_policies?: [{
		match?: Array<CaddyRouteMatch>
		certificate_selection?: {
			serial_number?: any 
			subject_organization?: Array<string>
			public_key_algorithm?: number
			any_tag?: Array<string>
			all_tags?: Array<string>
		}
		cipher_suites?: Array<string>,
		curves?: Array<string>,
		alpn?: Array<string>,
		protocol_min?: string 
		protocol_max?: string 
		client_authentication?: {
			trusted_ca_certs?: Array<string>
			trusted_ca_certs_pem_files?: Array<string>
			trusted_leaf_certs?: Array<string>
			mode?: string
		},
		default_sni?: string
	}]
	automatic_https?: {
		disable?: boolean
		disable_redirects?: boolean
		skip?: Array<string>
		skip_certificates?: Array<string>
		ignore_loaded_certificates?: boolean
	},
	strict_sni_host?: boolean 
	logs?: {
		default_logger_name?: "",
		logger_names?: {[key:string]: string}
		skip_hosts?: Array<string>
		skip_unmapped_hosts?: boolean
	}
	experimental_http3?: boolean
	allow_h2c?: boolean
}


export interface CaddyHttpRoute{
	group?: string 
	match?: Array<CaddyRouteMatch>,
	handle?: Array<CaddyHttpRouteHandler>,
	terminal?:  boolean
}

export interface CaddyTlsWrapper{
	tls?: {
		wrapper: 'tls'
	}
}

export type CaddyHttpRouteHandler = A.CaddyHttpEncode | A.CaddyHttpFileServer  | A.CaddyHttpHeaders | A.CaddyHttpMap | A.CaddyHttpMetrics | A.CaddyHttpPush | A.CaddyHttpRequestBody | A.CaddyHttpReverseProxy | A.CaddyHttpRewrite | A.CaddyHttpStaticResponse | A.CaddyHttpSubRoute | A.CaddyHttpVars | A.CaddyHttpError