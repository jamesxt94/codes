import {CaddyHeaders,CaddyHeadersActions} from './header'

import { CaddyHttpRouteHandler } from "./http";
import { CaddyRouteMatch } from "./routeMatch";
import { CaddySelectionPolicy } from "./selectionPolicy";
import { CaddyTransport } from "./transport";


export interface CaddyHttpEncode{
	handler: "encode"
	encodings?: {
		br? : {}
		gzip?: {
			level?: number
		}
		zstd?: {}
	}
	prefer?: Array<string>
	minimum_length?: number
	match?: {
		status_code?: Array<number>
		headers?: {[key:string]: Array<string>}
	}
}

export interface CaddyHttpError{
	handler: "error",
	error?: string
	status_code?: string 
}


export interface CaddyHttpFileServer{
	handler: "file_server",
	root?: string
	hide?: Array<string>
	index_names?: Array<string>
	browse?: {
		template_file?: string
	},
	canonical_uris?: boolean
	status_code?: string
	pass_thru?: boolean
	precompressed?: {
		br? : {}
		gzip?: {
			level?: number
		}
		zstd?: {}
	}
	precompressed_order?: Array<string>
}



export interface CaddyHttpHeaders extends CaddyHeaders{
	handler: "headers"
}


export interface CaddyHttpMap{
	handler: "map"
	source?: string,
	destinations?: Array<string>
	mappings?: [{
		input?: string
		input_regexp?: string
		outputs?: []
	}],
	defaults?: Array<string>
}

export interface CaddyHttpMetrics{
	handler: "metrics",
	disable_openmetrics?: boolean
}




export interface CaddyHttpPush{
	handler: "push"
	resources?: [{
		method?: string
		target?: string
	}]
	headers?: CaddyHeadersActions
}


export interface CaddyHttpRequestBody{
	handler: "request_body"
	max_size?: number
}




export interface CaddyHttpReverseProxy{
	handler: 'reverse_proxy'
	transport?: CaddyTransport
	circuit_breaker?: any
	load_balancing?: {
		selection_policy?: CaddySelectionPolicy
		try_duration?: number 
		try_interval?: number 
		retry_match?: Array<CaddyRouteMatch>
	}
	health_checks?: {
		active?: {
			path?: string 
			uri?: string 
			port?: number
			headers? : {[key: string]: Array<string>}
			interval?: number  
			timeout?: number 
			max_size?: number 
			expect_status?: number 
			expect_body?: string 
		}
		passive?: {
			fail_duration?: number 
			max_fails?: number 
			"unhealthy_request_count"?: number 
			"unhealthy_status"?: Array<number>
			"unhealthy_latency"?: number 
		}
	},
	upstreams?: [
		{
			dial?: string 
			lookup_srv?: string 
			max_requests?: number 
		}
	]
	flush_interval?: number 
	headers?: CaddyHeaders
	buffer_requests?: boolean
	buffer_responses?: boolean
	max_buffer_size?: number
	handle_response?: [{
		match?: {
			status_code?: Array<number>
			headers?: {[key: string]: Array<string>}
		},
		status_code?: string
		routes?: [{
			group?: string
			match?: [CaddyRouteMatch]
			handle?: [CaddyHttpRouteHandler]
			terminal?: boolean 
		}]
	}]
}

export interface CaddyHttpRewrite{
	handler: "rewrite",
	method?: string
	uri?: string 
	strip_path_prefix?: string
	strip_path_suffix?: string
	uri_substring?: [{
		find?: string 
		replace?: string 
		limit?: number
	}],
	path_regexp?: [{
		find?: string
		replace?: string
	}]
}


export interface CaddyHttpStaticResponse{
	handler: 'static_response'
	status_code?: string 
	headers?: {[key:string]: Array<string>}
	body?: string 
	close?: boolean
	abort?: boolean
}


export interface CaddyHttpSubRoute{
	handler: 'subroute'
	routes?: [{
		group?: string
		match?: Array<CaddyRouteMatch>
		handle?: Array<CaddyHttpRouteHandler>
		terminal?: boolean
	}],
	errors?: {
		routes?: [{
			group?: string
			match?: Array<CaddyRouteMatch>
			handle?: Array<CaddyHttpRouteHandler>
			terminal?: boolean
		}]
	}
}

type vars = {[key: string]: string}
export interface CaddyHttpVars extends vars{
	handler: 'vars'
}


