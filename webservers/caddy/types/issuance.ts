



export type CaddyIssuance = CaddyIssuanceAcme | CaddyIssuanceInternal | CaddyIssuanceZeroSsl

export interface CaddyIssuanceZeroSsl extends CaddyIssuanceCommon{
	module: 'zerossl'
}


export interface CaddyIssuanceInternal{
	module: 'internal'
	ca? : string
	lifetime?: number 
	sign_with_root?: boolean
}


export interface CaddyIssuanceAcme extends CaddyIssuanceCommon{
	module: 'acme'
}

export interface CaddyIssuanceCommon{
	ca? : string
	test_ca?: string
	email?: string
	account_key?: string
	external_account?: {
		key_id: string 
		mac_key: string 
	}
	acme_timeout?: number 
	challenges?: {
		http?: CaddyAcmeChallenge,
		"tls-alpn"?: CaddyAcmeChallenge,
		dns?: CaddyAcmeDnsChallenge
		bind_host?: string
	}
	trusted_roots_pem_files?: Array<string>,
	preferred_chains?: {
		smallest?: boolean 
		root_common_name?: Array<string>
		any_common_name?: Array<string>
	}
}


export interface CaddyAcmeChallenge{
	disabled?: boolean
	alternate_port: number
}

export interface CaddyAcmeDnsChallenge{
	provider?: any
	ttl?: number 
	propagation_timeout?: number 
	resolvers?: Array<string>
}
