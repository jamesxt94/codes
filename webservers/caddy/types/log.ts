import { CaddyEncoder } from "./encoder"

export interface CaddyLoggingConfig{
	sink?:{
		writer?: CaddyWriter
	}
	logs: {[key:string]: CaddyLog}
}

export interface CaddyLog{
	writer?: CaddyWriter
	encoder?: CaddyEncoder
	level? : string 
	sampling?: {
		interval?: number 
		first?: number
		thereafter?: number
	}
	include?: Array<string>
	exclude?: Array<string>
}

export interface CaddyWriterDiscard{
	output: "discard"
}
export interface CaddyWriterStderr{
	output: "stderr"
}
export interface CaddyWriterStdout{
	output: "stdout"
}
export interface CaddyWriterFile{
	output: "file"
	filename?: string 
	roll?: boolean
	roll_size_mb?: boolean 
	roll_gzip?: boolean
	roll_local_time?: boolean 
	roll_keep?: 0	
	roll_keep_days?: 0	
}
export interface CaddyWriterNet{
	output: "net"
	address?: string 
	dial_timeout?: number 
}
export type CaddyWriter = CaddyWriterDiscard | CaddyWriterStderr | CaddyWriterStdout | CaddyWriterFile