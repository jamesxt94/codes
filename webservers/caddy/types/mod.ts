import {CaddySelectionPolicy} from './selectionPolicy'
import { CaddyRouteMatch } from './routeMatch'
import {CaddyAdminConfig} from './admin'
import { CaddyLoggingConfig } from './log'
import { CaddyAppsConfig } from './apps'


export interface CaddyConfig {
	admin?: CaddyAdminConfig
	logging?: CaddyLoggingConfig
	storage?: {
		file_system?:{
			module:'file_system'
			root?: string 
		}
	}
	apps?: CaddyAppsConfig
}












