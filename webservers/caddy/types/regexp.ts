
export interface CaddyRegExpSearch{
	search?: string
	search_regexp?: string 
	replace?: string 
}


export interface CaddyRegExp{
	name?: string
	pattern?: string 
}