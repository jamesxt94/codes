import { CaddyRegExp } from "./regexp";

export interface CaddyRouteMatch{
	expression?: string
	header?: {[key:string] : Array<string>}
	header_regexp?: {[key:string] : CaddyRegExp}
	host?: Array<string>
	method?: Array<string>
	not?: Array<CaddyRouteMatch>
	path?: Array<string>
	path_regexp?: CaddyRegExp,
	protocol?: "http" | "https" | "grpc"
	query?: {[key:string] : Array<string>}
	remote_ip?: {
		ranges?: Array<string>
		forwarded?: boolean
	}
	vars_regexp?: {[key:string] : CaddyRegExp}
	vars?: {[key:string] : string}
	file?: {
		root?: string
		try_files?: Array<string >
		try_policy?: string 
		split_path?: Array<string>
	}

}