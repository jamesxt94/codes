
export interface CaddySelectionPolicyCookie{
	"policy": "cookie"
	"name"?: string 
	"secret"?: string
}

export interface CaddySelectionPolicyFirst{
	"policy": "first"
}

export interface CaddySelectionPolicyIpHash{
	"policy": "ip_hash"
}

export interface CaddySelectionPolicyLeastConn{
	"policy": "least_conn"
}

export interface CaddySelectionPolicyRoundRobin{
	"policy": "round_robin"
}

export interface CaddySelectionPolicyUriHash{
	"policy": "uri_hash"
}

export interface CaddySelectionPolicyHeader{
	"policy": "header"
	field: string 
}

export interface CaddySelectionPolicyRandomChoose{
	"policy": "random_choose"
	choose?: number
}

export interface CaddySelectionPolicyRandom{
	"policy": "random"
}


export type CaddySelectionPolicy = CaddySelectionPolicyCookie | CaddySelectionPolicyFirst | CaddySelectionPolicyIpHash | CaddySelectionPolicyLeastConn | CaddySelectionPolicyRoundRobin |  CaddySelectionPolicyUriHash | CaddySelectionPolicyHeader | CaddySelectionPolicyRandomChoose | CaddySelectionPolicyRandom