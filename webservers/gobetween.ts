import fetch from 'npm://node-fetch@3.2.0'
import Os from 'os'
import {Exception} from "gh+/kwruntime/std@1.1.14/util/exception.ts"
import * as async from "gh+/kwruntime/std@1.1.14/util/async.ts"
import Path from 'path'
import {pipeline} from 'stream'
import {promisify} from 'util'
import Child from 'child_process'
import {kawix} from 'gh+/kwruntime/core@2009992/src/kwruntime.ts'
import fs from 'fs'
import crypto from 'crypto'

/*
let configExample = {
	servers: {
		default: {
			protocol: 'tcp',
			bind: '0.0.0.0:33018',
			discovery: {
				kind: "static",
				static_list: [
					"localhost:8000 weight=5",
					"localhost:8000 weight=5"
				]
			}
		}
	}
}
*/

export class Program{

	static async main(){
		try{
			const res = await GoBetween.download()
			Child.spawn(res.bin, kawix.appArguments.slice(1), {
				stdio:'inherit'
			})	
		}catch(e){
			console.error("Failed executing:", e)
			process.exit(1)
		}
	}


}

export class GoBetween{

	static #download : any 

	async getCmd(config:any){

		let download= await GoBetween.download()
		let configFolder = Path.join(download.userfolder,"config")
		if(!fs.existsSync(configFolder)) await fs.promises.mkdir(configFolder)

		let name = "config.json"
		let keys = Object.keys(config.servers || {})
		if(keys.length){
			let serverc = config.servers[keys[0]]
			let t = serverc.bind || ''
			name = crypto.createHash("md5").update(t).digest("hex") + ".json"
		}

		let configFile = Path.join(configFolder, name)
		await fs.promises.writeFile(configFile, JSON.stringify(config))

		return {
			bin: download.bin,
			args: ["from-file", configFile, "-f","json"]
		}
	}

	async start(config:any, options?){
		let cmd = await this.getCmd(config)
		return Child.spawn(cmd.bin, cmd.args,Object.assign({
			stdio:'inherit'
		}, options || {}))
	}


	static async download(){
		if(this.#download) return this.#download
		let res = await fetch("https://api.github.com/repos/yyyar/gobetween/releases")
		let json = await res.json()
		
		let platform = Os.platform()
		let archConversions = {
			"x64": "amd64",
			"x86": "386",
			"ia32": "386"
		}
		let arch = archConversions[Os.arch()]
		let regex = new RegExp(`_${platform}_${arch}\.(.*)$`,"i")
		let usable = null 

		json.reverse()
		for(let item of json){
			let assets = item.assets
			for(let asset of assets){
				let match = asset.name.match(regex)
				if(match){
					usable = {
						item,
						asset
					}
					break 
				}
			}
		}

		if(!usable){
			throw Exception.create("There is not relase available for your platform.").putCode("RELEASE_UNAVAILABLE")
		}
		

		// 	user data folder
		let path = Path.join(Os.homedir(),".kawi","user-data")
		if(!fs.existsSync(path)) await fs.promises.mkdir(path)
		path = Path.join(path, "gobetween")
		if(!fs.existsSync(path)) await fs.promises.mkdir(path)
		let out = Path.join(path, usable.asset.name)
		let ok =  Path.join(path, usable.asset.name + ".ok.json")
		// ahora descomprimir 
		let bin = Path.join(path, "bin")
		if(!fs.existsSync(ok)){
			console.info(`Downloading ${usable.asset.name}`)

			res = await fetch(usable.asset.browser_download_url)
			const streamPipeline = promisify(pipeline)
			await streamPipeline(res.body, fs.createWriteStream(out))
			let stat = await fs.promises.stat(out)
			if(stat.size != usable.asset.size){
				throw Exception.create("Download failed. Invalid content size").putCode("CORRUPT_DOWNLOAD")
			}
			
			if(usable.asset.name.endsWith(".tar.gz")){
				if(!fs.existsSync(bin)) await fs.promises.mkdir(bin)
				let tar = await import("npm://tar@6.1.11")
				let extractor = tar.x({
					C: bin
				})
				let reader = fs.createReadStream(out)
				let def = new async.Deferred<void>()
				reader.once("error", def.reject)
				extractor.once("error", def.reject)
				extractor.once("finish", def.resolve)
				reader.pipe(extractor)
				await def.promise
			}
			else if(usable.asset.name.endsWith(".zip")){
				if(!fs.existsSync(bin)) await fs.promises.mkdir(bin)
				let zip = await import("npm://adm-zip@0.5.9")
				zip.extractAllTo(bin, true)
			}

			await fs.promises.writeFile(ok, Date.now().toString())
		}
		return this.#download = {
			folder:bin,
			userfolder: path,
			bin: Path.join(bin, "gobetween")
		}
		

	}

}